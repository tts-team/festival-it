;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                                       ;;
;;;                Centre for Speech Technology Research                  ;;
;;;                     University of Edinburgh, UK                       ;;
;;;                       Copyright (c) 1996,1997                         ;;
;;;                        All Rights Reserved.                           ;;
;;;                                                                       ;;
;;;  Permission is hereby granted, free of charge, to use and distribute  ;;
;;;  this software and its documentation without restriction, including   ;;
;;;  without limitation the rights to use, copy, modify, merge, publish,  ;;
;;;  distribute, sublicense, and/or sell copies of this work, and to      ;;
;;;  permit persons to whom this work is furnished to do so, subject to   ;;
;;;  the following conditions:                                            ;;
;;;   1. The code must retain the above copyright notice, this list of    ;;
;;;      conditions and the following disclaimer.                         ;;
;;;   2. Any modifications must be clearly marked as such.                ;;
;;;   3. Original authors' names are not deleted.                         ;;
;;;   4. The authors' names are not used to endorse or promote products   ;;
;;;      derived from this software without specific prior written        ;;
;;;      permission.                                                      ;;
;;;                                                                       ;;
;;;  THE UNIVERSITY OF EDINBURGH AND THE CONTRIBUTORS TO THIS WORK        ;;
;;;  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ;;
;;;  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ;;
;;;  SHALL THE UNIVERSITY OF EDINBURGH NOR THE CONTRIBUTORS BE LIABLE     ;;
;;;  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ;;
;;;  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ;;
;;;  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ;;
;;;  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ;;
;;;  THIS SOFTWARE.                                                       ;;
;;;                                                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                                       ;;
;;;                           Modified by:                                ;;
;;;                Istituto di Scienze e Tecnologie Cognitive             ;;
;;;               Sezione di Padova, Fonetica e Dialettologia             ;;
;;;                   Consiglio Nazionale delle Ricerche                  ;;
;;;                       Copyright (c) 2001,2007                         ;;
;;;                        All Rights Reserved.                           ;;
;;;                                                                       ;;
;;;                                                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;
;;;  Support for MBROLA as an external module.
;;;
;;; You might want to set this in your sitevars.scm

(require 'italian_mbrola_require)

;; Configure MBROLA synthesis
(defvar mbrola_progname "mbrola"
  "mbrola_progname
  The program name for mbrola.")
(defvar mbrola_database "fr1"
  "mbrola_database
 The name of the MBROLA database to use during MBROLA Synthesis.")
(defvar mbrola_flags "-e"
  "mbrola_flags
  The flags to use with mbrola.")

;; This is used by italian voices to find
;; mbrola database.
(if (getenv 'MBROLA_DBS)
    (defvar mbrola_dbs (getenv 'MBROLA_DBS))
    (defvar mbrola_dbs (path-append libdir "voices/italian/italian/"))
    )
;; The following is unusefull, 'cause it always
;; returns mbrola_dbs
(if (defvar mbrola_dbs)
    (define (return_mbrola_dbs) mbrola_dbs)
    (define (return_mbrola_dbs)(cdr (assoc current-voice voice-locations)))
    )

;; set TEMPORARY directory
(defvar temp_file_dir
  (if (getenv 'TEMP)
      (path-as-directory (getenv 'TEMP))
      (path-as-directory "/tmp/")
      )
  "temp_file_dir
  Temporary directory where mbrola synthesis
  stores some output files"
  )

(define (MBROLA_Synth utt)
  "(MBROLA_Synth UTT)
  Synthesize using MBROLA as external module.  Basically dump the info
  from this utterance. Call MBROLA and reload the waveform into utt.
  [see MBROLA]"

  (set! phoFileT  (string-append (make_tmp_filename) ".pho"))
  (set! wavFileT  (string-append (make_tmp_filename) ".wav"))
  (let ()
    (save_segments_mbrola utt phoFileT)
    (if (probe_file phoFileT)
      (begin
	(set! comando (string-append "\"" mbrola_progname "\" "
				mbrola_flags " "
				mbrola_database " \""
				phoFileT "\" \""
				wavFileT "\""))
    	;(print comando)
	(system comando)
	(utt.import.wave utt wavFileT)
	(apply_hooks after_synth_hooks utt)
	(delete-file phoFileT)
	(delete-file wavFileT)
	(if (defvar debug_mode) 
	  (if debug_mode 
	    (let ()
    		(print comando)
	  	(print (string-append "Synthesized file: " wavFileT))
		)
	    ) ; if debug_mode
	  ) ; end if defvar debug_mode

	utt)
	(print (string-append "File phonemes " phoFileT " not found"))
      ) ;; if probe_file phoFileT
    )
  )

(define (save_segments_mbrola utt filename)
  "(save_segments_mbrola UTT FILENAME)
  Save segment information in MBROLA format in filename.  The format is
  phone duration (ms) [% position F0 target]*. [see MBROLA]"

  (if (and (defvar text_mode) text_mode)
	(set! word_var_old " ")
      )
  (let ((fd (fopen filename "w")) new_seg_name)
    (mapcar
     (lambda (segment)
       (set! old_seg_name (item.feat segment 'name))  

       (if (or (equal? old_seg_name "#") (equal? old_seg_name "pau"))   ;Mod. x adattare anche alle voci inglesi
       	   (if (equal? old_seg_name "#")   				;Modifica per mappare # in _
	   (set! new_seg_name "_") (set! new_seg_name "pau"))
	   (begin
	     (set! new_seg_name old_seg_name)
	     
	     (if (and (defvar text_mode) text_mode) 
		 (begin
		   ;;(set! tempvar (item.next tempvar))
		   ;;(print (item.name segment))
		   (set! sylvar (item.relation.parent segment 'SylStructure)) 
		   (set! wordvar (item.relation.parent sylvar 'SylStructure)) 
		   (set! word_var_name (item.feat wordvar 'name))
		   
		   (set! semstruct (item.relation.parent wordvar 'SemStructure)) 	; ok
		   (set! affect (item.relation.parent wordvar 'Affective))
		   (if (equal? affect nil)
		       (set! tag_name (item.feat semstruct 'name))	; ok
		       ;;(set! tag_name (item.feat affect 'name))
		       (set! tag_name (item.feat affect 'type))
		       )  ; end if
		   
  		   (if (defvar debug_mode) 
      			(if debug_mode 
			(print (string-append "Segment= " old_seg_name "  Word= " word_var_name  "  tag= " tag_name))))

		   (if (equal? word_var_name word_var_old)
		       ()
		       (begin
  		   	(if (defvar debug_mode) 
      				(if debug_mode 
			 	(print (string-append "Previouvs word= " word_var_old " Current word= " word_var_name))))
			 (format fd "; word= %s   tag= %s " word_var_name  tag_name)
			 (terpri fd)
			 (set! word_var_old word_var_name)
			 )
		       )

		   )
		 )
	     ) ; fine begin
	   ) ; fine if
	 
       (set! par (item.relation.parent segment 'Segctrl))

       (save_seg_mbrola_entry 
	new_seg_name
	(item.feat segment 'segment_start)
	(item.feat segment 'segment_duration)
	(mapcar
	 (lambda (targ_item)
	   (list
	    (item.feat targ_item "pos")
	    (item.feat targ_item "f0")))
	 (item.relation.daughters segment 'Target)) ;; list of targets

	(if par 
	    (list 

	     (list "Vol" (item.feat par "Vol"))
	     (list "SpTilt" (item.feat par "SpTilt"))
	     (list "Shim" (item.feat par "Shim"))
	     (list "Jit" (item.feat par "Jit"))
	     (list "AspNoise" (item.feat par "AspNoise"))
	     (list "F0Flut" (item.feat par "F0Flut"))
	     (list "AmpFlut" (item.feat par "AmpFlut"))
	     (list "SpWarp" (item.feat par "SpWarp"))
	     ) ;fine list
	    ); fine if
	fd))
     (utt.relation.items utt 'Segment))
    (fclose fd)))


(define (save_seg_mbrola_entry name start dur targs emotive fd)
  "(save_seg_mbrola_entry ENTRY NAME START DUR TARGS EMOTIVE FD)
    Entry contains,
  (name duration num_targs start 1st_targ_pos 1st_targ_val emotive)
    where emotive is a list of low-level controls."
  (format fd "%s %d " name (nint (* dur 1000)))
  (if targs     ;; if there are any targets
      (mapcar
       (lambda (targ) ;; targ_pos and targ_val
	 (let ((targ_pos (car targ))
	       (targ_val (car (cdr targ))))

	   (format fd "%d %d " 
		   (nint (* 100 (/ (- targ_pos start) dur))) ;; % pos of target
		   (nint (parse-number targ_val)))           ;; target value
	   ))
       targs))
  
  (if (not(defvar novoiceq))
    (if emotive 
      (mapcar
       (lambda (x)
         (set! y (car (cdr x))) ;y = valore della feature
	 (if (not (equal? y 0)) (format fd "%s 0 %1.5f 100 %1.5f " (car x) y y) )
	 ) ;fine lambda
       emotive
       ); fine mapcar
      );fine if emotive
  );fine if novoiceq
  (terpri fd)
  (terpri fd)
)

(provide 'mbrola)
(provide 'italian_mbrola)
