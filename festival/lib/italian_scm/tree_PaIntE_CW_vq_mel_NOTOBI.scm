(set! carini_PaIntE_CW_vq_tree_2c '

((stress is 0)
((R:SylStructure.parent.R:Phrase.parent.type is interrogativa)
((0.598618 (9.68605 6.51109 -0.0536214 -54.8923 -91.5095 102.461)))
((p.stress is 0)
((0.800247 (3.33208 5.13545 0.717231 -13.2299 -73.353 98.7439)))
((lisp_position_in_phrase < 0.877155)
((lisp_next_stress < 5)
((R:SylStructure.parent.R:Word.pbreak is mB)
((ssyl_in < 2.5)
((0.850662 (15.3368 4.32572 -0.377587 -95.2183 -23.3212 103.064)))
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.882854)
((lisp_position_in_phrase < 0.183015)
((next_accent < 3)
((0.69985 (26.3271 3.60501 -0.152685 -42.7753 -103.29 121.763)))
((0.628997 (6.31594 4.22008 1.38717 41.7462 25.1827 154.381))))
((next_accent < 4.5)
((0.786095 (10.4199 4.98856 -0.30889 -30.3953 -11.6012 98.2498)))
((last_accent < 5.5)
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.835403)
((R:SylStructure.parent.R:Word.lisp_since_punctuation < 6.5)
((0.703804 (28.6232 4.54953 -0.116291 -19.748 -41.169 108.909)))
((syl_out < 21)
((0.467944 (8.84966 2.39545 -0.204429 -47.6878 -50.9698 100.332)))
((0.791058 (11.6135 7.9943 -0.343799 37.0594 43.4355 151.085)))))
((0.400949 (22.7569 8.10448 1.30212 35.9668 44.1844 180.667))))
((0.262685 (5.88722 16.5755 1.01511 55.769 30.0044 248.972))))))
((R:SylStructure.parent.R:Word.lisp_since_punctuation < 4.5)
((syl_in < 17.5)
((R:SylStructure.parent.R:Word.pos_in_phrase < 3.5)
((0.8713 (9.68605 6.51109 -0.0536214 -54.8923 -91.5095 102.461)))
((0.342886 (18.6639 6.8211 -0.511651 78.3908 77.2349 215.563))))
((0.733996 (9.68605 6.51109 -0.0536214 -54.8923 -91.5095 102.461))))
((0.733679 (8.84966 2.39545 -0.204429 -47.6878 -50.9698 100.332))))))
((0.891904 (11.6135 7.9943 -0.343799 37.0594 43.4355 151.085))))
((0.287708 (26.3271 3.60501 -0.152685 -42.7753 -103.29 121.763))))
((0.683013 (8.84966 2.39545 -0.204429 -47.6878 -50.9698 100.332))))))
((R:SylStructure.parent.R:Word.pbreak is NB)
((R:SylStructure.parent.R:Phrase.parent.type is interrogativa)
((R:SylStructure.parent.R:Word.content_words_in < 0.5)
((R:SylStructure.parent.R:Word.p.gpos is dadj_pos)
((0.210229 (5.88722 16.5755 1.01511 55.769 30.0044 248.972)))
((syl_in < 0.5)
((0.354092 (7.46296 5.71342 0.772318 99.5393 34.2731 235.151)))
((R:SylStructure.parent.R:Word.n.gpos is content)
((next_accent < 5.5)
((0.691832 (24.0765 9.08074 0.34678 6.51835 21.4431 293.933)))
((0.162204 (5.86625 5.24649 0.739252 -168.605 -19.3854 114.282))))
((R:SylStructure.parent.R:Word.n.tPOS is V)
((0.566303 (24.0765 9.08074 0.34678 6.51835 21.4431 293.933)))
((R:SylStructure.parent.R:Word.p.pos_in_phrase < 0.5)
((0.674011 (4.04297 6.18189 1.13721 -94.7184 -20.6557 163.502)))
((R:SylStructure.parent.R:Word.n.tPOS is ACR)
((0.76603 (24.0765 9.08074 0.34678 6.51835 21.4431 293.933)))
((R:SylStructure.parent.R:Word.p.tPOS is V)
((0.659875 (5.86625 5.24649 0.739252 -168.605 -19.3854 114.282)))
((0.782963 (24.0765 9.08074 0.34678 6.51835 21.4431 293.933))))))))))
((0.71939 (9.5396 5.5075 0.611311 54.8241 48.9315 252.262))))
((R:SylStructure.parent.R:Word.p.gpos is pre_pp)
((0.77504 (6.64511 7.32339 1.01331 -51.4365 -16.7171 236.802)))
((R:SylStructure.parent.R:Word.word_numsyls < 6.5)
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.250208)
((syl_in < 2.5)
((0.829543 (5.88722 16.5755 1.01511 55.769 30.0044 248.972)))
((syl_numphones < 2.5)
((R:SylStructure.parent.R:Word.p.gpos is conjcoo_corr)
((0.842333 (4.60571 17.3027 0.612133 38.1189 32.5853 149.985)))
((R:SylStructure.parent.R:Word.word_numsyls < 1.5)
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.0232558)
((0.437969 (5.28097 26.9723 1.45935 -49.7812 -18.1229 144.087)))
((nn.stress is 0)
((0.686126 (11.6135 7.9943 -0.343799 37.0594 43.4355 151.085)))
((0.573145 (4.80846 27.6886 1.32524 -30.9834 -19.7913 102.647)))))
((R:SylStructure.parent.R:Word.n.pbreak is mB)
((0.826949 (4.60571 17.3027 0.612133 38.1189 32.5853 149.985)))
((0.917867 (25.3523 7.15133 0.541526 83.8651 83.4604 228.29))))))
((asyl_in < 12.5)
((R:SylStructure.parent.R:Word.lisp_until_punctuation < 11.5)
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.224638)
((R:SylStructure.parent.R:Word.p.gpos is dadj_pos)
((0.64512 (24.9068 26.29 0.192404 -17.4379 -19.7927 105.737)))
((R:SylStructure.parent.R:Word.content_words_in < 1.5)
((sub_phrases < 0.5)
((R:SylStructure.parent.R:Word.lisp_until_punctuation < 7.5)
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.108226)
((0.882922 (26.6758 4.59751 0.632068 -23.3167 -20.7806 98.0176)))
((syl_in < 7.5)
((0.772428 (8.39525 25.4907 -0.365928 18.2165 19.3068 126.434)))
((0.499872 (23.6416 8.84444 0.89209 -26.7183 -78.3158 107.845)))))
((0.558163 (5.25348 21.9728 0.930051 81.9507 43.9737 204.382))))
((0.176102 (5.88722 16.5755 1.01511 55.769 30.0044 248.972))))
((lisp_position_in_phrase < 0.681986)
((R:SylStructure.parent.R:Phrase.parent.type is completativa)
((0.757981 (25.0757 28.9456 -0.612082 2.33531 8.99638 117.544)))
((pp.syl_break is 0)
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.0195019)
((0.57574 (25.5298 8.00331 -0.226173 29.4944 117.169 244.305)))
((syl_out < 14)
((0.431956 (25.3523 7.15133 0.541526 83.8651 83.4604 228.29)))
((syl_in < 13.5)
((0.630741 (8.47338 26.6701 0.766625 -58.1751 -68.6752 105.547)))
((0.848386 (5.5764 26.4373 1.55707 25.0793 21.6172 137.854))))))
((R:SylStructure.parent.R:Word.words_out < 11.5)
((0.790178 (24.9068 26.29 0.192404 -17.4379 -19.7927 105.737)))
((0.777157 (8.06005 13.8754 0.610121 89.0273 120.163 255.518))))))
((0.956509 (23.6416 8.84444 0.89209 -26.7183 -78.3158 107.845))))))
((0.884204 (27.2774 24.2743 1.59946 14.5738 19.003 137.525))))
((0.902129 (27.9763 6.02327 -0.347303 -22.9643 -46.3499 167.07))))
((0.399761 (10.3065 6.21992 0.227883 -139.084 -131.117 124.933))))))
((R:SylStructure.parent.R:Word.lisp_Simbolic_word_end_punct is 0)
((R:SylStructure.parent.R:Word.n.gpos is prono_ind)
((0.770661 (26.0886 26.6778 0.491019 45.7739 31.8697 176.087)))
((R:SylStructure.parent.R:Word.word_numsyls < 2.5)
((R:SylStructure.parent.R:Word.p.pbreak is B)
((0.301558 (5.25348 21.9728 0.930051 81.9507 43.9737 204.382)))
((R:SylStructure.parent.R:Word.words_out < 42)
((last_accent < 9.5)
((R:SylStructure.parent.R:Word.p.tPOS is B)
((R:SylStructure.parent.R:Word.lisp_since_punctuation < 4.5)
((0.907472 (25.0757 28.9456 -0.612082 2.33531 8.99638 117.544)))
((0.698206 (28.8194 5.64394 0.401359 21.4417 40.4631 159.669))))
((R:SylStructure.parent.R:Word.p.pbreak is mB)
((syl_in < 22)
((R:SylStructure.parent.R:Word.lisp_until_punctuation < 7.5)
((0.65351 (5.25348 21.9728 0.930051 81.9507 43.9737 204.382)))
((0.711233 (1.96174 3.9913 -0.440597 -3.2447 -55.9203 115.529))))
((0.871817 (28.8194 5.64394 0.401359 21.4417 40.4631 159.669))))
((R:SylStructure.parent.R:Word.n.tPOS is ACR)
((0.835039 (24.9068 26.29 0.192404 -17.4379 -19.7927 105.737)))
((p.stress is 1)
((0.906419 (26.0886 26.6778 0.491019 45.7739 31.8697 176.087)))
((0.889766 (27.2774 24.2743 1.59946 14.5738 19.003 137.525)))))))
((0.838449 (26.4662 18.9806 -0.443697 35.7879 34.363 157.915))))
((0.525702 (24.9068 26.29 0.192404 -17.4379 -19.7927 105.737)))))
((syl_out < 5.5)
((lisp_position_in_phrase < 0.645833)
((syl_onset_type is -V)
((0.939539 (6.26202 4.62517 0.514794 48.5022 30.2529 164.569)))
((0.495925 (24.9068 26.29 0.192404 -17.4379 -19.7927 105.737))))
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.267262)
((0.398158 (10.3065 6.21992 0.227883 -139.084 -131.117 124.933)))
((0.892236 (6.29582 27.8839 0.445086 22.098 18.5869 126.582)))))
((R:SylStructure.parent.R:Word.lisp_since_punctuation < 14.5)
((R:SylStructure.parent.R:Word.p.gpos is v_dpv_in_pp)
((0.909629 (7.21608 27.9783 0.895133 40.5965 22.7116 214.129)))
((ssyl_out < 22.5)
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.996063)
((R:SylStructure.parent.R:Word.content_words_out < 12.5)
((R:SylStructure.parent.R:Word.lisp_since_punctuation < 10.5)
((ssyl_in < 24.5)
((R:SylStructure.parent.R:Word.word_numsyls < 3.5)
((R:SylStructure.parent.R:Word.content_words_in < 12.5)
((R:SylStructure.parent.R:Word.p.gpos is art_indef)
((0.868261 (9.5396 5.5075 0.611311 54.8241 48.9315 252.262)))
((R:SylStructure.parent.R:Word.lisp_until_punctuation < 3.5)
((R:SylStructure.parent.R:Word.pos_in_phrase < 21)
((R:SylStructure.parent.R:Word.tPOS is A)
((0.959987 (6.31594 4.22008 1.38717 41.7462 25.1827 154.381)))
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.372382)
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.362058)
((0.894657 (28.6232 4.54953 -0.116291 -19.748 -41.169 108.909)))
((0.532064 (7.21608 27.9783 0.895133 40.5965 22.7116 214.129))))
((R:SylStructure.parent.R:Phrase.parent.type is dichiarativa)
((0.829366 (8.06005 13.8754 0.610121 89.0273 120.163 255.518)))
((0.824113 (28.6232 4.54953 -0.116291 -19.748 -41.169 108.909))))))
((0.224372 (8.06005 13.8754 0.610121 89.0273 120.163 255.518))))
((ssyl_out < 19.5)
((R:SylStructure.parent.R:Word.p.tPOS is nn)
((0.77759 (26.6758 4.59751 0.632068 -23.3167 -20.7806 98.0176)))
((R:SylStructure.parent.R:Word.p.word_numsyls < 2.5)
((lisp_position_in_phrase < 0.726666)
((0.895499 (26.0886 26.6778 0.491019 45.7739 31.8697 176.087)))
((0.818422 (22.7569 8.10448 1.30212 35.9668 44.1844 180.667))))
((0.745488 (25.3523 7.15133 0.541526 83.8651 83.4604 228.29)))))
((0.734373 (25.3523 7.15133 0.541526 83.8651 83.4604 228.29))))))
((0.3269 (7.12457 11.1062 1.04578 -18.8298 -21.6899 100.103))))
((asyl_out < 0.5)
((0.392801 (5.5764 26.4373 1.55707 25.0793 21.6172 137.854)))
((asyl_out < 8.5)
((R:SylStructure.parent.R:Phrase.parent.type is esplicativa)
((0.806723 (5.88722 16.5755 1.01511 55.769 30.0044 248.972)))
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.44534)
((R:SylStructure.parent.R:Word.tPOS is B)
((0.846166 (4.67251 9.80021 0.975414 -55.6069 -16.8203 101.246)))
((last_accent < 5.5)
((0.779041 (5.25348 21.9728 0.930051 81.9507 43.9737 204.382)))
((0.90081 (28.6232 4.54953 -0.116291 -19.748 -41.169 108.909)))))
((R:SylStructure.parent.R:Word.lisp_until_punctuation < 11.5)
((R:SylStructure.parent.R:Word.p.tPOS is A)
((0.861843 (26.3813 24.4183 0.0901984 -17.9805 -33.6362 159.225)))
((R:SylStructure.parent.R:Word.p.word_numsyls < 4.5)
((lisp_position_in_phrase < 0.676956)
((0.892001 (26.0886 26.6778 0.491019 45.7739 31.8697 176.087)))
((nn.syl_break is 1)
((0.653776 (28.5047 5.34462 0.428085 25.9289 30.1033 195.51)))
((0.795821 (5.72828 28.1602 0.317089 -25.7987 -18.4792 106.353)))))
((0.852733 (8.17229 5.6827 1.74047 -15.0042 -3.93713 124.903)))))
((0.282502 (8.06005 13.8754 0.610121 89.0273 120.163 255.518))))))
((0.904233 (5.25348 21.9728 0.930051 81.9507 43.9737 204.382))))))
((R:SylStructure.parent.R:Word.lisp_until_punctuation < 5.5)
((0.913673 (26.4662 18.9806 -0.443697 35.7879 34.363 157.915)))
((0.627408 (5.88722 16.5755 1.01511 55.769 30.0044 248.972)))))
((0.846113 (4.60571 17.3027 0.612133 38.1189 32.5853 149.985))))
((0.567935 (4.80846 27.6886 1.32524 -30.9834 -19.7913 102.647))))
((0.773477 (6.29582 27.8839 0.445086 22.098 18.5869 126.582))))
((0.834689 (8.06005 13.8754 0.610121 89.0273 120.163 255.518)))))
((0.807337 (7.12457 11.1062 1.04578 -18.8298 -21.6899 100.103)))))))
((0.799524 (25.5298 8.00331 -0.226173 29.4944 117.169 244.305)))))
((0.246186 (7.21608 27.9783 0.895133 40.5965 22.7116 214.129))))))
((R:SylStructure.parent.R:Word.lisp_Simbolic_word_end_punct is F)
((R:SylStructure.parent.R:Word.lisp_since_punctuation < 2.5)
((0.708471 (5.86625 5.24649 0.739252 -168.605 -19.3854 114.282)))
((0.705051 (4.67251 9.80021 0.975414 -55.6069 -16.8203 101.246))))
((R:SylStructure.parent.R:Word.p.tPOS is A)
((last_accent < 9)
((next_accent < 2.5)
((R:SylStructure.parent.R:Word.n.gpos is conjcoo_cop)
((0.337771 (27.9951 26.7249 0.423952 15.0571 20.8065 130.256)))
((pp.syl_break is 1)
((0.710059 (4.67251 9.80021 0.975414 -55.6069 -16.8203 101.246)))
((R:SylStructure.parent.R:Word.p.pos_in_phrase < 11.5)
((0.81476 (4.80846 27.6886 1.32524 -30.9834 -19.7913 102.647)))
((0.49636 (8.06005 13.8754 0.610121 89.0273 120.163 255.518))))))
((0.847936 (8.10456 4.31678 0.461389 -14.8671 -21.412 95.0297))))
((0.229597 (25.3523 7.15133 0.541526 83.8651 83.4604 228.29))))
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.882854)
((position_type is mid)
((R:SylStructure.parent.R:Word.lisp_Simbolic_word_end_punct is H)
((0.440631 (27.9951 26.7249 0.423952 15.0571 20.8065 130.256)))
((R:SylStructure.parent.R:Phrase.parent.lisp_phrasepos_in_utt < 0.121118)
((0.83805 (6.32608 3.83716 0.625275 -76.4247 -12.0199 100.409)))
((0.870515 (16.6329 11.5936 0.560597 13.7833 15.6671 117.255)))))
((ssyl_out < 2.5)
((sub_phrases < 0.5)
((0.838583 (4.80846 27.6886 1.32524 -30.9834 -19.7913 102.647)))
((0.607692 (10.4207 7.68094 0.483423 47.9418 66.268 175.251))))
((0.899999 (24.9533 25.9934 1.18084 -22.6838 -20.4681 117.334)))))
((R:SylStructure.parent.R:Word.tPOS is V)
((0.883043 (27.9753 6.79329 0.411557 17.6783 21.7125 131.253)))
((0.879094 (10.4207 7.68094 0.483423 47.9418 66.268 175.251)))))))))



)