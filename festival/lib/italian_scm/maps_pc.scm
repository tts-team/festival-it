;;
; In questo file sono definite le funzioni e i coefficienti di
; combinazione lineare per la mappatura tra i vari tag. I coefficienti
; variano da 0.00 a 1.00.
;
; VINCOLI IMPORTANTI:
; Vanno rispettate alcune regole per la scrittura di questo file.
; 1) E' possibile applicare una mappatura verso uno stesso tag mediante pi�
; funzioni diverse; se lo si fa la lista di funzioni va raggruppata per
; nome di tag.
; 2) Dove presenti, i tag di medio livello (M):
;       modal, soft, loud, pressed, breathy, whispery, harsh, tense, hoarse, creaky, tremulous
;       hypernasal, hyponasal
;    devono precedere quelli
;    di basso livello (L):
;       volume, sptilt, shimmer, jitter, f0flut, ampflut, aspnoise, spwarp
; 3) Il numero massimo di funzioni usabili per la mappatura di un tipo 
; di tag � cinque (#define FNZ_SIZE 5 in maps.cc). 
; 4) E' ammessa una sola mappatura per i tag di basso livello verso
; s� stessi (nel passaggio da parola a segmento).
; ANNOTAZIONE:
; 5) La mappatura da tag di medio livello a tag di basso livello pu� avvenire
; usando una qualsiasi delle funzioni disponibili. Va notato per� che, se 
; si usano files APML come input, tale funzione verr� applicata a tag di medio
; livello contententi una sola parola (vedere codice...). Questo comporta che se
; f(0) (primo val. della funzione per la mappatura) � zero tale sar� anche il
; livello di tutti i tag di basso livello ottenuti.
;

(set! map
 (list ;;insieme dei tags
   (list ;; tags di alto livello
     (list
       'anger
       ;(list 'M 'hat 1.0 'loud)
       ;	(list 'L 'hat 0.88 'volume)
       ;	(list 'L 'hat 0.7 'sptilt)
       ;(list 'M 'hat 0.3 'harsh)
       ;	(list 'L 'hat 0.9 'jitter)
       ;	(list 'L 'hat 0.0 'shimmer)
       (list 'L 'hat 0.81 'volume)
       (list 'L 'hat 0.0 'jitter)
       (list 'L 'hat 0.8 'sptilt)
       (list 'L 'hat 0.3 'spwarp)
     )
     (list
       'disgust
       ;(list 'M 'hat 0.1 'harsh)
       (list 'L 'hat 0.8  'spwarp)
       (list 'L 'hat 0.5 'sptilt)
     )
     (list
       'joy
       ;(list 'M 'hat 1.0 'loud) 
       ;(list 'M 'hat 0.5 'breathy)
	 (list 'L 'hat 0.78 'volume)
       (list 'L 'hat 0.8 'spwarp)
       (list 'L 'hat 0.75 'sptilt)
       

     )
     (list
       'fear
       ;(list 'M 'hat 0.95 'tremulous)
       ;	(list 'L 'hat 0.85 'volume)
       ;	(list 'L 'hat 0.9 'f0flut)
       ;	(list 'L 'hat 0.3 'spwarp)
       ;(list 'M 'hat 0.7 'breathy)
       ;	(list 'L 'hat 0.05 'sptilt)
       ;	(list 'L 'hat 0.2 'aspnoise)

       	(list 'L 'hat 0.79 'volume)
       	(list 'L 'hat 0.5 'f0flut)
       	(list 'L 'hat 0.3 'spwarp)
       	(list 'L 'hat 0.4 'sptilt)
       	(list 'L 'hat 0.1 'aspnoise)
     )
     (list
       'sadness
       ;(list 'M 'hat 0.2 'breathy)
       ;		(list 'L 'hat 0.05 'sptilt)
       ;		(list 'L 'hat 0.2 'aspnoise)
	 (list 'L 'hat 0.75 'volume)
       (list 'L 'hat 0.4 'sptilt)
       (list 'L 'hat 0.2 'spwarp)
       (list 'L 'hat 0.2 'f0flut)
       (list 'L 'hat 0.2 'ampflut)
        )
     (list
       'surprise
        ;(list 'M 'hat 1.0 'loud) 
       ;(list 'M 'hat 0.5 'breathy)
       
       (list 'L 'hat 0.4 'sptilt)
       (list 'L 'hat 0.1 'aspnoise)

       (list 'L 'hat 0.9 'spwarp)
       (list 'L 'hat 0.8 'volume)
     )
     (list
       'distress
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'embarassment
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'happy-for
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'gloating
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'resentment
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'relief
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'jealousy
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'envy
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'sorry-for
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'hope
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'satisfaction
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'fear-confirmed
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'disappointment
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'pride
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'shame
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'reproach
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'liking
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'disliking
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'gratitude
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'gratification
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'remorse
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'love
       (list 'M 'hat 1.0 'modal)
     )
     (list
       'hate
       (list 'M 'hat 1.0 'modal)
     )
   )
   (list ;;tags medio livello N.B.: Vedere nota 5!!
     (list 
       'modal
       (list 'L 'hat 0.88 'volume)
     )
     (list 
       'soft
       (list 'L 'hat 0.8 'volume)
       (list 'L 'hat 0.3 'sptilt)
     )
     (list 
       'loud
       (list 'L 'hat 0.88 'volume)
       (list 'L 'hat 0.7 'sptilt)
     )
     (list 
       'pressed
       (list 'L 'hat 0.8 'volume)
       (list 'L 'hat 0.75 'sptilt)
     )
     (list 
       'breathy
       (list 'L 'hat 0.05 'sptilt)
       (list 'L 'hat 0.2 'aspnoise)
     )
     (list 
       'whispery
       (list 'L 'hat 1.0 'aspnoise)
       ;(list 'L 'hat 0.5 'sptilt)
       (list 'L 'hat 0.8 'volume)

     )
     (list 
       'harsh
       (list 'L 'hat 0.9 'jitter)
       (list 'L 'hat 0.0 'shimmer)
     )
     (list 
       'tense
       (list 'L 'hat 0.7 'sptilt)
     )
     (list 
       'hoarse
       (list 'L 'hat 0.3 'jitter)
       (list 'L 'hat 0.1 'shimmer)
       (list 'L 'hat 0.2 'aspnoise)	
       (list 'L 'hat 0.84 'volume)      
     )
     (list 
       'creaky
       (list 'L 'hat 0.5 'volume)
     )
     (list 
       'tremulous
       (list 'L 'hat 0.85 'volume)
       (list 'L 'hat 0.9 'f0flut)
       (list 'L 'hat 0.3 'spwarp)
       ;(list 'L 'hat 0.2 'aspnoise)	
     )
     (list 
       'hypernasal
       (list 'L 'hat 0.5 'volume)
     )
     (list 
       'hyponasal
       (list 'L 'hat 0.5 'volume)
     )
   )
   (list ;mappatura lowlevel->lowlevel
     (list
       'volume
       (list 'L 'dyn 1.0 'volume)
     )
     (list
       'sptilt
       (list 'L 'dyn 1.0 'sptilt)
     )
     (list
       'shimmer
       (list 'L 'dyn 1.0 'shimmer)
     )
     (list
       'jitter
       (list 'L 'dyn 1.0 'jitter)
     )
     (list
       'f0flut
       (list 'L 'dyn 1.0 'f0flut)
     )
     (list
       'ampflut
       (list 'L 'dyn 1.0 'ampflut)
     )
     (list
       'aspnoise
       (list 'L 'dyn 1.0 'aspnoise)
     )
     (list
       'spwarp
       (list 'L 'dyn 1.0 'spwarp)
     )
   )
 )
)

;(provide 'maps)
