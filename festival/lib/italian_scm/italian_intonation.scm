;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                        ;;
;;                Istituto di Fonetica e Dialettologia                    ;;
;;                 Consiglio Nazionale delle Ricerche                     ;;
;;                            Padova Italy                                ;;
;;                        Copyright (c) 2000                              ;;
;;                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        						  ;;
;;           Centro per la ricerca scientifica e tecnologica              ;;
;;         	             Povo (TN) - Italy				  ;;
;;			    Copyright (c) 2000                            ;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Reegole per l'Intonazione 
;;;
;;;;;;;;;;;;;;;;;;
;;; Cart-tree per l'intonazione valutato in base alla cifra che 
;;; contraddistingue l'accento della sillaba
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
(set! spanish_accent_cart_tree
  '
  (
   (R:SylStructure.parent.gpos is content)
    ( (stress is 1)
       ((Accented))
       ((NONE))
    )
  )
) 

(set! italian_accent_cart_tree
  '
  (
   (pos_in_word is 0);; prima sillaba della parola
     ( (R:SylStructure.parent.p.FW is 0);;prima parola della utterance.
       ( (R:SylStructure.parent.FW is content)
    		( (stress is 1)
       			((Accented_I));; Accento iniziale
       			((Inizio_content));; Inizio senza accento
       			)
       		((primaFW)) 
       		)
       	
       	( (R:SylStructure.parent.FW is content)
       		(  (R:SylStructure.parent.p.pbreak is B) ;;prima parola dopo  brek B.
       			( (stress is 1)
       				((Accented_I))
       				((Inizio_content))
       				)
       			( (R:SylStructure.parent.p.pbreak is BB) ;;prima parola dopo  brek BB.
       				( (stress is 1)
       					((Accented_I))
       					((Inizio_content))
       					)
       				( (stress is 1)
       					((Accented))
       					((NONE))
       					)
       				)
       			)
       			
       		(  (R:SylStructure.parent.p.pbreak is B) ;;prima parola dopo  brek B.
      				((primaFW))
      				( (R:SylStructure.parent.p.pbreak is BB) ;;prima parola dopo  brek BB.
      					((primaFW))
      					( (R:SylStructure.parent.p.FW is content)
      						((primaFW))
      						( (R:SylStructure.parent.p.apo is 1)
      							((primaFW))
      							((altreFW))
      							)
      						)
      					)
      				
       			)	
      		)	    
   )
  ( (R:SylStructure.parent.FW is content)
    	( (stress is 1)
       		((Accented))
       		((NONE))
       		)
       	((NONE))
 	)  
    )
)


;;;
;;;  Using the general intonation module add TRINGULAR accents
;;;  of Accented syllables
;;;

(define (targ_func_fw_q utt syl )
 "Regole per l'intonazione: gruppo di intonazione: Function Word. Inoltre gestisce le frasi interrogative"
   (let ((start (item.feat syl 'syllable_start))
        (end (item.feat syl 'syllable_end))
        ;;CONDIZIONE-TEMPO DI INZIO GRUPPO DI RESPIRO
        (br_start_fw (item.feat syl "R:GrRespiro.parent.daughter1.R:SylStructure.daughter1.segment_start" ))
        ;;CONDIZIONE-TEMPO DI FINE GRUPPO DI RESPIRO
        (br_end_fw (item.feat syl "R:GrRespiro.parent.daughtern.R:SylStructure.daughtern.segment_end" ))
        ;(item_word (item.relation.parent syl 'SylStructure))
        ;Frasi: condizione-tempo di frase 
        (br_start (item.feat syl "R:SylStructure.parent.R:Phrase.parent.daughter1.R:SylStructure.daughter1.daughter1.segment_start" ))
        (br_end (item.feat syl "R:SylStructure.parent.R:Phrase.parent.daughtern.R:SylStructure.daughtern.daughtern.segment_end" ))
       
	fustart fuend fstart fend fmed)
         
        (set! tmed (+ start (/ (- end start) 2))) ;punto di mezzo tra start e end
        (set! ph_pr_len (- br_end br_start))
        (set! cfb 0.7) ;;il coeficiente cfb serve per l'inclinazione finale a fine phrase break
        ;;;;;;;;; FREQUENZE DI RIFERIMENTO ;;;;;;;;;;;;;; 
        (set! fustart '140)
        (set! fuend   '60)
        (set! fuend_q '140)
        (set! fstartFW 120) ;Valore sopra il quale non va la f0 del secondo gruppo intonativo
        (set! rise 1.2)
        (set! fstart_lapse_FW 90) ;Valore sopra il quale non va la f0 dell' ultima sillaba prima del secondo gruppo intonativo
        (set! lapse1 0.85)
        (set! lapse2 0.8)
        ;;; COEFFICIENTI PER LE INTERROGATIVE ;;;;;;;;
        (set! mr 4)
        (set! i_lapse 0.9)
        (set! i_rise 1.05)
        
	;;CALCOLO COEFFICIENTI DELLA BASE LINE
        (set! m_base (* (/ (- fuend fustart) ph_pr_len) cfb))
	(set! y0 (- fustart (* m_base br_start)))
	;;;;;;;;;;;;;;;;;;;;;;;
	(set! fstart (+ y0 (* m_base start)))
	(set! fmed (+ y0 (* m_base tmed)))
		 
	(set! t1 start)
        (set! t2 tmed)
        
  (if (string-equal (item.feat syl 'R:SylStructure.parent.R:Phrase.parent.type) "interrogativa")
   	(let ()
   	  ;(print "Question")
   	  ;;;; CONDIZIONE DI ULTIMA SILLABA TONICA
   	  ;(print (item.feat syl 'ssyl_out))	;;;; 0
   	  ;(print (item.feat syl 'stress))	;;;; 1 
   	  ;;;; CONDIZIONE DI sillaba precedente ULTIMA SILLABA TONICA
   	  ;(print (item.feat (item.next syl) 'ssyl_out))	;;;; 0
   	  ;(print (item.feat (item.next syl) 'stress ));;;; 1
   	  
   	    	   
  ;;;;;;;;;;;;;;;;;; QUESTION ;;;;;;;;;;;;;;;;;
  (cond 
  	((eq? (item.feat syl 'syl_in ) 0) ;inizio frase 
  		(let () 
  			(set! f1 fustart)
  			(set! f2 fmed)))
        ((eq? (item.feat syl 'syl_out ) 0) ;fine frase
  		(let () 
  			(set! f1 fstart)
  			;(set! t2 end)
  			;(set! f2 fuend_q)
  			))
  	((and (eq? (item.feat syl 'ssyl_out ) 0) (eq? (item.feat syl 'stress) 1)) ;ultima sillaba tonica
  		(let () 
  			(set! ph (item.relation.daughter1 syl 'SylStructure))
   	   		;(while (or (not ph) (string-equal (item.feat ph 'ph_vc) "-"))
   	   		(while  (string-equal (item.feat ph 'ph_vc) "-")
   	   		(set! ph (item.next ph)))
   	   		(set! 0q (+ (item.feat syl 'syl_vowel_start) (* 0.1 (- (item.feat ph 'end) (item.feat syl 'syl_vowel_start)))))
   	   		(set! tq (+ (item.feat syl 'syl_vowel_start) (* 0.75 (- (item.feat ph 'end) (item.feat syl 'syl_vowel_start)))))
   	   		(set! t1 0q)
   	   		(set! t2 tq)
   	   		(set! f1 (* (+ y0 (* m_base 0q)) i_lapse))
  			(set! f2 (* (+ y0 (* m_base tq)) i_lapse))))
    	
  	((and (eq? (item.feat (item.next syl) 'ssyl_out ) 0) (eq? (item.feat (item.next syl) 'stress) 1)) ;sillaba precedente l'ultima sillaba tonica
  		(let () 
  			
  			(set! ph (item.relation.daughter1 syl 'SylStructure))
   	   		(while (string-equal (item.feat ph 'ph_vc) "-")
   	   		(set! ph (item.next ph)))
   	   		(set! t2 (item.feat ph 'end))
   	   		(set! f1 fstart)
  			(set! f2 (* (+ y0 (* m_base t2)) i_rise))))
  	((eq? (item.feat (item.next syl) 'ssyl_out ) 0) ;dopo l'ultima sillaba tonica
  		(let ()
  			(set! s syl)   ;;ritrova l'ultima syllaba tonica
  			(while (not (and (eq? (item.feat s 'ssyl_out ) 0) (eq? (item.feat s 'stress) 1)))
  			(set! s (item.prev s)))
  			(set! ph (item.relation.daughter1 s 'SylStructure)) ;;ritrova la vocale
   	   		(while (string-equal (item.feat ph 'ph_vc) "-")
   	   		(set! ph (item.next ph)))
   	   		(set! tq (+ (item.feat s 'syl_vowel_start) (* 0.75 (- (item.feat ph 'end) (item.feat s 'syl_vowel_start)))))
   	   		(set! fc (* (+ y0 (* m_base tq)) i_lapse))
   	   		(set! y0r (- fc (* mr tq)))
   	   		(set! f1 (+ y0r (* mr t1)))
   	   		(set! f2 (+ y0r (* mr t2)))))
  	(t 				   ;tutti gli altri casi 	
  		(let ()
  			(set! f1 fstart)
  			(set! f2 fmed))))
   
 ;; Se fa parte di un gruppo intonativo ma NON a inizio frase...
 ;;
 ;; Vecchia condizione pi� leggibile ma non compatibile con 131:
 ;; ;;;;(not (equal? (item.feat syl "R:SylStructure.parent.R:Phrase.parent.daughter1.R:SylStructure.daughter1.id") (item.feat syl "R:GrRespiro.parent.daughter1.id")))

  (if (and (equal? (item.feat syl "R:GrRespiro.parent.name") "primaFW") (not (equal? (item.relation.daughter1 (item.relation.daughter1 (item.relation.parent (item.relation.parent syl 'SylStructure) 'Phrase) 'Phrase) 'SylStructure) (item.relation (item.relation.daughter1 (item.relation.parent syl 'GrRespiro) 'GrRespiro) 'SylStructure))) )
 	
 	
 	(let () 
 		(set! fstart_fw  (+ y0 (* m_base br_start_fw)))
 		(set! fp1 (* fstart_fw rise))
 		
 		;(print "RISE")
 		(if (<= fp1 fstartFW)
 			(let ()
 				;(print "Original")
 				(set! f1 (* f1 rise))
 				(set! f2 (* f2 rise)))
 			(let ()
 				(set! k (/ fstartFW fstart_fw))
 				;(print "KAPPA RISE")
 				(set! f1 (* f1 k))
 				(set! f2 (* f2 k))))))
 ;;La syllaba successiva fa parte di un nuovo intonation group
 ;;
 ;; Vecchia condizione pi� leggibile ma non compatibile con 131:
 ;;(not (equal? (item.feat (item.next syl) "R:SylStructure.parent.R:Phrase.parent.daughter1.R:SylStructure.daughter1.id") (item.feat (item.next syl) "R:GrRespiro.parent.daughter1.id")))
 
 (if (and (item.next syl) (equal? (item.feat (item.next syl) "R:Intonation.daughter1.name") "primaFW") (equal? (item.feat (item.next syl) "R:GrRespiro.parent.name") "primaFW") (not (equal? (item.relation.daughter1 (item.relation.daughter1 (item.relation.parent (item.relation.parent (item.next syl) 'SylStructure) 'Phrase) 'Phrase) 'SylStructure) (item.relation (item.relation.daughter1 (item.relation.parent (item.next syl) 'GrRespiro) 'GrRespiro) 'SylStructure))) )
 	(let () 
		
		;(print "LAPSE")
		(set! fd1 (* f1 lapse1))
		(set! fd2 (* f2 lapse2))
		(if (>= fd2 fstart_lapse_FW)
			(let ()
				(set! k (/ fstart_lapse_FW f2))
				;(print "KAPPA LAPSE")
				(set! f1 (* f1 k))
				(set! f2 (* f2 k)))
			(let ()
				;(print "Original")
				(set! f1 fd1)
				(set! f2 fd2)))))

;;Accento si ma non nell'ultima sillaba tonica
  (if (and (not (and (eq? (item.feat syl 'ssyl_out ) 0) (eq? (item.feat syl 'stress) 1)))(or (equal? (item.feat syl "R:Intonation.daughter1.name") "Accented")(equal? (item.feat syl "R:Intonation.daughter1.name") "Accented_I")))
	(let ()
 		(set! f1 (+ f1 3))
 		(set! f2 (+ f2 8))))
  (if (eq? (item.feat syl 'syl_out ) 0) ;fine frase
  		(let () 
  			;(set! f1 fstart)
  			(set! t2 end)
  			(set! f2 fuend_q))))
 ;;;;;;;;;;;;;;;;;;;;;;;;;; FINE QUESTION ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 (let ()
 ;;;;;;;;;;;;;;;;;;;;;;;;; NORMAL ;;;;;;;;;;;;;;;;;;;;;;;		 	   
  (cond 
  	((eq? (item.feat syl 'syl_in ) 0) ;inizio frase 
  		(let () 
  			(set! f1 fustart)
  			(set! f2 fmed)))
        ((eq? (item.feat syl 'syl_out ) 0) ;fine frase
  		(let () 
  			(set! f1 fstart)
  			;(set! t2 end)
  			;(set! f2 fuend)
  			))
  	(t 				   ;tutti gli altri casi 	
  		(let ()
  			(set! f1 fstart)
  			(set! f2 fmed))))
   
 ;; Se fa parte di un gruppo intonativo ma NON a inizio frase...
 ;;
 ;; Vecchia condizione pi� leggibile ma non compatibile con 131:
 ;; ;;;;(not (equal? (item.feat syl "R:SylStructure.parent.R:Phrase.parent.daughter1.R:SylStructure.daughter1.id") (item.feat syl "R:GrRespiro.parent.daughter1.id")))

  (if (and (equal? (item.feat syl "R:GrRespiro.parent.name") "primaFW") (not (equal? (item.relation.daughter1 (item.relation.daughter1 (item.relation.parent (item.relation.parent syl 'SylStructure) 'Phrase) 'Phrase) 'SylStructure) (item.relation (item.relation.daughter1 (item.relation.parent syl 'GrRespiro) 'GrRespiro) 'SylStructure))) )
 	
 	
 	(let () 
 		(set! fstart_fw  (+ y0 (* m_base br_start_fw)))
 		(set! fp1 (* fstart_fw rise))
 		
 		;(print "RISE")
 		(if (<= fp1 fstartFW)
 			(let ()
 				;(print "Original")
 				(set! f1 (* f1 rise))
 				(set! f2 (* f2 rise)))
 			(let ()
 				(set! k (/ fstartFW fstart_fw))
 				;(print "KAPPA RISE")
 				(set! f1 (* f1 k))
 				(set! f2 (* f2 k))))))
 ;;La syllaba successiva fa parte di un nuovo intonation group
 ;;
 ;; Vecchia condizione pi� leggibile ma non compatibile con 131:
 ;;(not (equal? (item.feat (item.next syl) "R:SylStructure.parent.R:Phrase.parent.daughter1.R:SylStructure.daughter1.id") (item.feat (item.next syl) "R:GrRespiro.parent.daughter1.id")))
 
 (if (and (item.next syl) (equal? (item.feat (item.next syl) "R:Intonation.daughter1.name") "primaFW") (equal? (item.feat (item.next syl) "R:GrRespiro.parent.name") "primaFW") (not (equal? (item.relation.daughter1 (item.relation.daughter1 (item.relation.parent (item.relation.parent (item.next syl) 'SylStructure) 'Phrase) 'Phrase) 'SylStructure) (item.relation (item.relation.daughter1 (item.relation.parent (item.next syl) 'GrRespiro) 'GrRespiro) 'SylStructure))) )
 	(let () 
		
		;(print "LAPSE")
		(set! fd1 (* f1 lapse1))
		(set! fd2 (* f2 lapse2))
		(if (>= fd2 fstart_lapse_FW)
			(let ()
				(set! k (/ fstart_lapse_FW f2))
				;(print "KAPPA LAPSE")
				(set! f1 (* f1 k))
				(set! f2 (* f2 k)))
			(let ()
				;(print "Original")
				(set! f1 fd1)
				(set! f2 fd2)))))

  (if (or (equal? (item.feat syl "R:Intonation.daughter1.name") "Accented")(equal? (item.feat syl "R:Intonation.daughter1.name") "Accented_I"))
	(let ()
 		(set! f1 (+ f1 3))
 		(set! f2 (+ f2 8))))
 		
 (if (eq? (item.feat syl 'syl_out ) 0) ;fine frase
 	(let () 
 		(set! t2 end)
 		(set! f2 fuend)))))

  ;(print (list t1 f1))
  ;(print (list t2 f2))
  	
  (list 
  	(list t1 f1)
  	(list t2 f2)) 
))

;;;RAGAZZA

(define (targ_func_fw_q_fem utt syl )
 "Regole per l'intonazione: gruppo di intonazione: Function Word. Inoltre gestisce le frasi interrogative"
   (let ((start (item.feat syl 'syllable_start))
        (end (item.feat syl 'syllable_end))
        ;;CONDIZIONE-TEMPO DI INZIO GRUPPO DI RESPIRO
        (br_start_fw (item.feat syl "R:GrRespiro.parent.daughter1.R:SylStructure.daughter1.segment_start" ))
        ;;CONDIZIONE-TEMPO DI FINE GRUPPO DI RESPIRO
        (br_end_fw (item.feat syl "R:GrRespiro.parent.daughtern.R:SylStructure.daughtern.segment_end" ))
        ;(item_word (item.relation.parent syl 'SylStructure))
        ;Frasi: condizione-tempo di frase 
        (br_start (item.feat syl "R:SylStructure.parent.R:Phrase.parent.daughter1.R:SylStructure.daughter1.daughter1.segment_start" ))
        (br_end (item.feat syl "R:SylStructure.parent.R:Phrase.parent.daughtern.R:SylStructure.daughtern.daughtern.segment_end" ))
       
	fustart fuend fstart fend fmed)
         
        (set! tmed (+ start (/ (- end start) 2))) ;punto di mezzo tra start e end
        (set! ph_pr_len (- br_end br_start))
        (set! cfb 0.7) ;;il coeficiente cfb serve per l'inclinazione finale a fine phrase break
        ;;;;;;;;; FREQUENZE DI RIFERIMENTO ;;;;;;;;;;;;;; 
        (set! fustart '220)
        (set! fuend   '180)
        (set! fuend_q '220)
        (set! fstartFW 210) ;Valore sopra il quale non va la f0 del secondo gruppo intonativo
        (set! rise 1.2)
        (set! fstart_lapse_FW 190) ;Valore sopra il quale non va la f0 dell' ultima sillaba prima del secondo gruppo intonativo
        (set! lapse1 0.85)
        (set! lapse2 0.8)
        ;;; COEFFICIENTI PER LE INTERROGATIVE ;;;;;;;;
        (set! mr 4)
        (set! i_lapse 0.9)
        (set! i_rise 1.05)
        
	;;CALCOLO COEFFICIENTI DELLA BASE LINE
        (set! m_base (* (/ (- fuend fustart) ph_pr_len) cfb))
	(set! y0 (- fustart (* m_base br_start)))
	;;;;;;;;;;;;;;;;;;;;;;;
	(set! fstart (+ y0 (* m_base start)))
	(set! fmed (+ y0 (* m_base tmed)))
		 
	(set! t1 start)
        (set! t2 tmed)
        
  (if (string-equal (item.feat syl 'R:SylStructure.parent.R:Phrase.parent.type) "interrogativa")
   	(let ()
   	  ;(print "Question")
   	  ;;;; CONDIZIONE DI ULTIMA SILLABA TONICA
   	  ;(print (item.feat syl 'ssyl_out))	;;;; 0
   	  ;(print (item.feat syl 'stress))	;;;; 1 
   	  ;;;; CONDIZIONE DI sillaba precedente ULTIMA SILLABA TONICA
   	  ;(print (item.feat (item.next syl) 'ssyl_out))	;;;; 0
   	  ;(print (item.feat (item.next syl) 'stress ));;;; 1
   	  
   	    	   
  ;;;;;;;;;;;;;;;;;; QUESTION ;;;;;;;;;;;;;;;;;
  (cond 
  	((eq? (item.feat syl 'syl_in ) 0) ;inizio frase 
  		(let () 
  			(set! f1 fustart)
  			(set! f2 fmed)))
        ((eq? (item.feat syl 'syl_out ) 0) ;fine frase
  		(let () 
  			(set! f1 fstart)
  			;(set! t2 end)
  			;(set! f2 fuend_q)
  			))
  	((and (eq? (item.feat syl 'ssyl_out ) 0) (eq? (item.feat syl 'stress) 1)) ;ultima sillaba tonica
  		(let () 
  			(set! ph (item.relation.daughter1 syl 'SylStructure))
   	   		;(while (or (not ph) (string-equal (item.feat ph 'ph_vc) "-"))
   	   		(while  (string-equal (item.feat ph 'ph_vc) "-")
   	   		(set! ph (item.next ph)))
   	   		(set! 0q (+ (item.feat syl 'syl_vowel_start) (* 0.1 (- (item.feat ph 'end) (item.feat syl 'syl_vowel_start)))))
   	   		(set! tq (+ (item.feat syl 'syl_vowel_start) (* 0.75 (- (item.feat ph 'end) (item.feat syl 'syl_vowel_start)))))
   	   		(set! t1 0q)
   	   		(set! t2 tq)
   	   		(set! f1 (* (+ y0 (* m_base 0q)) i_lapse))
  			(set! f2 (* (+ y0 (* m_base tq)) i_lapse))))
    	
  	((and (eq? (item.feat (item.next syl) 'ssyl_out ) 0) (eq? (item.feat (item.next syl) 'stress) 1)) ;sillaba precedente l'ultima sillaba tonica
  		(let () 
  			
  			(set! ph (item.relation.daughter1 syl 'SylStructure))
   	   		(while (string-equal (item.feat ph 'ph_vc) "-")
   	   		(set! ph (item.next ph)))
   	   		(set! t2 (item.feat ph 'end))
   	   		(set! f1 fstart)
  			(set! f2 (* (+ y0 (* m_base t2)) i_rise))))
  	((eq? (item.feat (item.next syl) 'ssyl_out ) 0) ;dopo l'ultima sillaba tonica
  		(let ()
  			(set! s syl)   ;;ritrova l'ultima syllaba tonica
  			(while (not (and (eq? (item.feat s 'ssyl_out ) 0) (eq? (item.feat s 'stress) 1)))
  			(set! s (item.prev s)))
  			(set! ph (item.relation.daughter1 s 'SylStructure)) ;;ritrova la vocale
   	   		(while (string-equal (item.feat ph 'ph_vc) "-")
   	   		(set! ph (item.next ph)))
   	   		(set! tq (+ (item.feat s 'syl_vowel_start) (* 0.75 (- (item.feat ph 'end) (item.feat s 'syl_vowel_start)))))
   	   		(set! fc (* (+ y0 (* m_base tq)) i_lapse))
   	   		(set! y0r (- fc (* mr tq)))
   	   		(set! f1 (+ y0r (* mr t1)))
   	   		(set! f2 (+ y0r (* mr t2)))))
  	(t 				   ;tutti gli altri casi 	
  		(let ()
  			(set! f1 fstart)
  			(set! f2 fmed))))
   
 ;; Se fa parte di un gruppo intonativo ma NON a inizio frase...
 ;;
 ;; Vecchia condizione pi� leggibile ma non compatibile con 131:
 ;; ;;;;(not (equal? (item.feat syl "R:SylStructure.parent.R:Phrase.parent.daughter1.R:SylStructure.daughter1.id") (item.feat syl "R:GrRespiro.parent.daughter1.id")))

  (if (and (equal? (item.feat syl "R:GrRespiro.parent.name") "primaFW") (not (equal? (item.relation.daughter1 (item.relation.daughter1 (item.relation.parent (item.relation.parent syl 'SylStructure) 'Phrase) 'Phrase) 'SylStructure) (item.relation (item.relation.daughter1 (item.relation.parent syl 'GrRespiro) 'GrRespiro) 'SylStructure))) )
 	
 	
 	(let () 
 		(set! fstart_fw  (+ y0 (* m_base br_start_fw)))
 		(set! fp1 (* fstart_fw rise))
 		
 		;(print "RISE")
 		(if (<= fp1 fstartFW)
 			(let ()
 				;(print "Original")
 				(set! f1 (* f1 rise))
 				(set! f2 (* f2 rise)))
 			(let ()
 				(set! k (/ fstartFW fstart_fw))
 				;(print "KAPPA RISE")
 				(set! f1 (* f1 k))
 				(set! f2 (* f2 k))))))
 ;;La syllaba successiva fa parte di un nuovo intonation group
 ;;
 ;; Vecchia condizione pi� leggibile ma non compatibile con 131:
 ;;(not (equal? (item.feat (item.next syl) "R:SylStructure.parent.R:Phrase.parent.daughter1.R:SylStructure.daughter1.id") (item.feat (item.next syl) "R:GrRespiro.parent.daughter1.id")))
 
 (if (and (item.next syl) (equal? (item.feat (item.next syl) "R:Intonation.daughter1.name") "primaFW") (equal? (item.feat (item.next syl) "R:GrRespiro.parent.name") "primaFW") (not (equal? (item.relation.daughter1 (item.relation.daughter1 (item.relation.parent (item.relation.parent (item.next syl) 'SylStructure) 'Phrase) 'Phrase) 'SylStructure) (item.relation (item.relation.daughter1 (item.relation.parent (item.next syl) 'GrRespiro) 'GrRespiro) 'SylStructure))) )
 	(let () 
		
		;(print "LAPSE")
		(set! fd1 (* f1 lapse1))
		(set! fd2 (* f2 lapse2))
		(if (>= fd2 fstart_lapse_FW)
			(let ()
				(set! k (/ fstart_lapse_FW f2))
				;(print "KAPPA LAPSE")
				(set! f1 (* f1 k))
				(set! f2 (* f2 k)))
			(let ()
				;(print "Original")
				(set! f1 fd1)
				(set! f2 fd2)))))

;;Accento si ma non nell'ultima sillaba tonica
  (if (and (not (and (eq? (item.feat syl 'ssyl_out ) 0) (eq? (item.feat syl 'stress) 1)))(or (equal? (item.feat syl "R:Intonation.daughter1.name") "Accented")(equal? (item.feat syl "R:Intonation.daughter1.name") "Accented_I")))
	(let ()
 		(set! f1 (+ f1 3))
 		(set! f2 (+ f2 8))))
  (if (eq? (item.feat syl 'syl_out ) 0) ;fine frase
  		(let () 
  			;(set! f1 fstart)
  			(set! t2 end)
  			(set! f2 fuend_q))))
 ;;;;;;;;;;;;;;;;;;;;;;;;;; FINE QUESTION ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 (let ()
 ;;;;;;;;;;;;;;;;;;;;;;;;; NORMAL ;;;;;;;;;;;;;;;;;;;;;;;		 	   
  (cond 
  	((eq? (item.feat syl 'syl_in ) 0) ;inizio frase 
  		(let () 
  			(set! f1 fustart)
  			(set! f2 fmed)))
        ((eq? (item.feat syl 'syl_out ) 0) ;fine frase
  		(let () 
  			(set! f1 fstart)
  			;(set! t2 end)
  			;(set! f2 fuend)
  			))
  	(t 				   ;tutti gli altri casi 	
  		(let ()
  			(set! f1 fstart)
  			(set! f2 fmed))))
   
 ;; Se fa parte di un gruppo intonativo ma NON a inizio frase...
 ;;
 ;; Vecchia condizione pi� leggibile ma non compatibile con 131:
 ;; ;;;;(not (equal? (item.feat syl "R:SylStructure.parent.R:Phrase.parent.daughter1.R:SylStructure.daughter1.id") (item.feat syl "R:GrRespiro.parent.daughter1.id")))

  (if (and (equal? (item.feat syl "R:GrRespiro.parent.name") "primaFW") (not (equal? (item.relation.daughter1 (item.relation.daughter1 (item.relation.parent (item.relation.parent syl 'SylStructure) 'Phrase) 'Phrase) 'SylStructure) (item.relation (item.relation.daughter1 (item.relation.parent syl 'GrRespiro) 'GrRespiro) 'SylStructure))) )
 	
 	
 	(let () 
 		(set! fstart_fw  (+ y0 (* m_base br_start_fw)))
 		(set! fp1 (* fstart_fw rise))
 		
 		;(print "RISE")
 		(if (<= fp1 fstartFW)
 			(let ()
 				;(print "Original")
 				(set! f1 (* f1 rise))
 				(set! f2 (* f2 rise)))
 			(let ()
 				(set! k (/ fstartFW fstart_fw))
 				;(print "KAPPA RISE")
 				(set! f1 (* f1 k))
 				(set! f2 (* f2 k))))))
 ;;La syllaba successiva fa parte di un nuovo intonation group
 ;;
 ;; Vecchia condizione pi� leggibile ma non compatibile con 131:
 ;;(not (equal? (item.feat (item.next syl) "R:SylStructure.parent.R:Phrase.parent.daughter1.R:SylStructure.daughter1.id") (item.feat (item.next syl) "R:GrRespiro.parent.daughter1.id")))
 
 (if (and (item.next syl) (equal? (item.feat (item.next syl) "R:Intonation.daughter1.name") "primaFW") (equal? (item.feat (item.next syl) "R:GrRespiro.parent.name") "primaFW") (not (equal? (item.relation.daughter1 (item.relation.daughter1 (item.relation.parent (item.relation.parent (item.next syl) 'SylStructure) 'Phrase) 'Phrase) 'SylStructure) (item.relation (item.relation.daughter1 (item.relation.parent (item.next syl) 'GrRespiro) 'GrRespiro) 'SylStructure))) )
 	(let () 
		
		;(print "LAPSE")
		(set! fd1 (* f1 lapse1))
		(set! fd2 (* f2 lapse2))
		(if (>= fd2 fstart_lapse_FW)
			(let ()
				(set! k (/ fstart_lapse_FW f2))
				;(print "KAPPA LAPSE")
				(set! f1 (* f1 k))
				(set! f2 (* f2 k)))
			(let ()
				;(print "Original")
				(set! f1 fd1)
				(set! f2 fd2)))))

  (if (or (equal? (item.feat syl "R:Intonation.daughter1.name") "Accented")(equal? (item.feat syl "R:Intonation.daughter1.name") "Accented_I"))
	(let ()
 		(set! f1 (+ f1 3))
 		(set! f2 (+ f2 8))))
 		
 (if (eq? (item.feat syl 'syl_out ) 0) ;fine frase
 	(let () 
 		(set! t2 end)
 		(set! f2 fuend)))))

  ;(print (list t1 f1))
  ;(print (list t2 f2))
  	
  (list 
  	(list t1 f1)
  	(list t2 f2)) 
))


(provide 'italian_intonation)
