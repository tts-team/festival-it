(set! SOLEML nil) 
(set! APML nil) 

;t Syllabe quando � gi� stata fatta la predizione, quando � usata nel cart training 
;nil quando � usta nella voce ; 
;IMS
(def_feature_docstring 'Phrase.lisp_phrase_num_syls
  "Phrase.lisp_num_syls
  Returns the number of syllables in a phrase as defined by the phrase relation. (GM)")
(define (phrase_num_syls phrase)
  (let ((sylcount 0))
    (mapcar
     (lambda (w)
       (let ((wrd (item.relation w 'Word)))
	 (mapcar
	  (lambda (s)
	   (set! sylcount(+ 1 sylcount)) )
	  (item.relation.daughters wrd 'SylStructure))))
     (item.daughters phrase))
    sylcount))

(def_feature_docstring 'Phrase.lisp_phrasepos_in_utt
  "Phrase.lisp_phrasepos_in_utt
  Returns the position of the phrase in the utterance (0 for first and 1 for last). (GM)")
(define (phrasepos_in_utt phrase)
  (let ((count -1)
	(pos -1)
	(ph (utt.relation.first (item.get_utt phrase) 'Phrase))) ; first phrase
    (while ph
       (set! count(+ 1 count))
       (if (equal? (item.feat phrase 'addr) (item.feat ph 'addr)) 
	   (set! pos count))
       (set! ph (item.next ph)))
    (cond
     ((equal? count 0) 0)
     (t
      (/ pos count)))))

(def_feature_docstring 'Syllable.lisp_position_in_phrase
  "Syllable.lisp_position_in_phrase
  Returns the position of the syl in the phrase (0 is initial, 1 is final). (GM)")
(define (position_in_phrase syl)
  (let ((ph_num_syl 
	 (item.feat syl 'R:SylStructure.parent.R:Phrase.parent.lisp_phrase_num_syls)))
    (cond
     ((eq ph_num_syl 0) 0)
     ((eq ph_num_syl 1) 0)
     (t
      (/ (item.feat syl 'syl_in) (- ph_num_syl 1))))))




(define (Accent_boundary_syl syl)
  ""
  (cond 
					;accento
   ((or 
     (string-equal (item.feat syl 'R:Intonation.daughter1.name) "Accented")
     (string-equal (item.feat syl 'R:Intonation.daughter1.name) "Accented_I"))
    (set! out "s*"))
					;boundaries
   ((or 
     (and (not (string-equal (item.feat syl 'R:SylStructure.parent.pbreak) "NB")) (string-equal (item.feat syl 'R:SylStructure.parent.daughtern.id) (item.feat syl 'id))) 
     (and (not (string-equal (item.feat syl 'R:SylStructure.parent.punct) "0")) (string-equal (item.feat syl 'R:SylStructure.parent.daughtern.id) (item.feat syl 'id))))		
 (set! out "s%"))
					;altrimenti
   (t 
    (set! out "s")))
  out)



(define (Accent_syl_X syl) ;al posto di: Accenti_intonativi_syl 
  "Ritorna l'accento intonativo tobi della parola il car in quanto in una sillaba pu� al massimo contenere un accento intonativo e un tono di confine (quelli che non contengono ne - ne % ne !)"
(item.name (item.relation.daughter1 syl 'Intonation)))

(define (Boundary_syl_X syl) ; al posto di: Boundary_syl
 "Ritorna la lista dei boundaries tone  tobi della sillaba (sono quelli che non (non contengono ne - ne % ne ! ))"
(item.name (item.relation.daughtern syl 'Intonation)))



;;WORD-------
(define (BI_word word)
  "Ritorna il BI della parola"
  (if (item.relation word 'ToBI)
      (let ( (lista (item.daughters (item.relation word 'ToBI))) elemBI BI)
	(while lista 
	       (set! elemBI (item.feat (car lista) 'BI))
	       
	       (if (eq? elemBI 0) 
		   (set! BI 1) ;setta BI = 0 e passa al prossimo item
		   (let () 
		     (if (string-equal elemBI "HiF0")
			 (set! BI 1) ;setta BI = 0 e passa al prossimo item
			 (set! BI elemBI)))) ;setta BI = elemBI e passa al prossimo item
	       (set! lista (cdr lista))) 
	(read-from-string BI))
      1));se non esiste Tobi ritorna 1

(define (Tones_List_word word)
  "Ritorna la lista di Toni tobi della parola"
  (let ( (lista (item.daughters (item.relation word 'ToBI))) elemTo LTones)
    (while lista 
	   (set! elemTo (item.feat (car lista) 'To))
	   (if (not (eq? elemTo 0)) 
	       (set! LTones (append LTones (list elemTo))))
	   (set! lista (cdr lista))) 
    LTones))

(define (HiF0_word word)
  "Ritorna t se la parola contiene HiF0 nil atrimenti"
  (let ( (lista (item.daughters (item.relation word 'ToBI))) elemBI LTones (return nil))
    (while lista 
	   (set! elemBI (item.feat (car lista) 'BI))
	   (if (string-equal elemBI "HiF0")
	       (set! return t))
	   (set! lista (cdr lista))) 
    return))


(define (Accenti_intonativi_word word)
  "Ritorna la lista delgli accenti intonativi tobi della parola (sono quelli che non contengono ne - ne % ne ! )"
  (let ( (lista (item.daughters (item.relation word 'ToBI))) elemTo LTones)
    (while lista 
	   (set! elemTo (item.feat (car lista) 'To))
	   (if (and (not (eq? elemTo 0)) (not (string-matches elemTo ".*%.*")) (not (string-matches elemTo ".*-.*")) (not (string-matches elemTo ".*!.*")))
	       (set! LTones (append LTones (list elemTo))))
	   (set! lista (cdr lista))) 
    LTones))

(define (Boundary_word word)
  "Ritorna la lista dei boundaries tone  tobi della parola (sono quelli che non (non contengono ne - ne % ne ! ))"
  (let ( (lista (item.daughters (item.relation word 'ToBI))) elemTo LTones)
    (while lista 
	   (set! elemTo (item.feat (car lista) 'To))
	   (if  (and (not (eq? elemTo 0)) (or (string-matches elemTo ".*%.*") (string-matches elemTo ".*-.*") (string-matches elemTo ".*!.*")))
	       (set! LTones (append LTones (list elemTo))))
	   (set! lista (cdr lista))) 
    LTones))

(define (Toni_confine_word word)
  "Ritorna la lista dei toni di confine tobi della parola (sono quelli che finiscono per % )"
  (let ( (lista (item.daughters (item.relation word 'ToBI))) elemTo LTones)
    (while lista 
	   (set! elemTo (item.feat (car lista) 'To))
	   (if (and (not (eq? elemTo 0)) (string-matches elemTo ".*%"))
	       (set! LTones (append LTones (list elemTo))))
	   (set! lista (cdr lista))) 
    LTones))

(define (Accenti_frase_word word)
  "Ritorna la lista dei toni di confine tobi della parola (sono quelli che finiscono per - o inizianoo per %)"
  (let ( (lista (item.daughters (item.relation word 'ToBI))) elemTo LTones)
    (while lista 
	   (set! elemTo (item.feat (car lista) 'To))
	   (if (and (not (eq? elemTo 0)) (string-matches elemTo ".*-") (string-matches elemTo "%.*"))
	       (set! LTones (append LTones (list elemTo))))
	   (set! lista (cdr lista))) 
    LTones))

(define (Downstep_word word)
  "Ritorna la lista dei donstep della parola (sono quelli che iniziano con ! "
  (let ( (lista (item.daughters (item.relation word 'ToBI))) elemTo LTones)
    (while lista 
	   (set! elemTo (item.feat (car lista) 'To))
	   (if (and (not (eq? elemTo 0)) (string-matches elemTo "!.*") )
	       (set! LTones (append LTones (list elemTo))))
	   (set! lista (cdr lista))) 
    LTones))

;---------Syntax WORD
(define (Syntax_Profondita_word word)
  "Profondit� dell albero synt in quel punto"
  (let ( (prof 1) (padre (item.relation.parent word 'Syntax)))
    (while (item.parent padre) 
	   (set! prof (+ prof 1))
	   (set! padre (item.parent padre)))
    prof))

(define (since_tree_word word)  
  "Attenzione Multi WORD! numero di parole dalla dall'inizio della sentence precedente intesa nel senso di albero."
  (cond 
   ((null word) 0);;inzio frase
   ((equal? (item.relation word 'Syntax) (item.daughter1 (item.relation.parent word 'Syntax))) 0);;primo figlio
   (t 
    (+ 1 (since_tree_word (item.prev word))))))

(define (until_tree_word word)  
  "Attenzione Multi WORD! numero di parole dalla fine della sentence precedente intesa nel senso di albero."
  (cond 
   ((null word) 0);; fine frase
   ((equal? (item.relation word 'Syntax) (item.daughtern (item.relation.parent word 'Syntax))) 0);;ultimo figlio
   (t 
    (+ 1 (until_tree_word (item.next word))))))

(define (until_tree_parziale_word word)  
  "Attenzione Multi WORD! numero di parole dalla fine della sentence precedente intesa nel senso di albero.considera le diramazioni parziali"
  (cond 
   ((null word) 0);; fine frase
   ((equal? (item.relation word 'Syntax) (item.daughtern (item.relation.parent word 'Syntax))) 0);;ultimo figlio
   ((not (equal? (item.relation.parent (item.next word) 'Syntax) (item.relation.parent word 'Syntax))) 0);diramazione parziale
   (t 
    (+ 1 (until_tree_parziale_word (item.next word))))))

(define (multi_word word)  
  "ritorna il tipo di multi word Exapansion o Compression"
  (let ( (r 0) (m (item.feat word 'MW)))
    (if (string-equal m "expansion")
	(if (not (string-equal (item.feat (item.next word) 'MW) "expansion"))
	    (set! r 'last_expansion)
	    (set! r 'E)))
    (if (string-equal m "compression")
	(set! r 'C))
    r))

;----------Other  WORD
(define (content_word word)  
  "1 if word is a content word defined in gpos 0 other wise"
  (read-from-string (item.feat word 'contentp)))

(define (since_punctuation word)
  "(since_punctuation word)
Number of words since last punctuation or beginning of utterance."
  (cond
   ((null (item.prev word)) 0) ;; beginning or utterance
   ((not (string-equal "0" (item.feat word "p.lisp_word_end_punct"))) 0)
   (t
    (+ 1 (since_punctuation (item.prev word))))))

(define (until_punctuation word)
  "(until_punctuation word)
Number of words until next punctuation or end of utterance."
  (cond
   ((null (item.next word)) 0) ;; ending or utterance
   ((not (string-equal "0" (word_end_punct word))) 0)
   (t
    (+ 1 (until_punctuation (item.next word))))))

(define (word_end_punct word)
  "(word_end_punct WORD)
punctuation at end of related WORD, otherwise 0."
(cond  
 (SOLEML  
  (item.feat word 'punct))
 (APML  
  (item.feat word 'punc))
 (t
  (token_end_punc word))))

(define (word_start_prepunctuation word)
  "(word_start_prepunctuation WORD)
Prepunctuation at start of related WORD otherwise 0."
(if (or SOLEML APML)
  (let ((p (item.feat word 'prepunctuation)))
    (cond
     ((equal? p "") "0")
     (t p)))
  (token_start_punc word))) 

(define (Phrase_Type utt)
  "Crea la features type nella relation Phrase V2"
  (let (p pu)
    (set! p (utt.relation.first utt 'Phrase))
    (while p
	   (set! pu (item.feat p "R:Phrase.daughtern.R:Word.lisp_Simbolic_word_end_punct"))
	   (cond
	    ((string-equal pu "A")
	       (item.set_feat p 'type "dichiarativa"))
	    ((string-equal pu "B")
	     (item.set_feat p 'type "completativa"))
	    ((string-equal pu "D")
	     (item.set_feat p 'type "esplicativa"))
	    ((string-equal pu "C")
	     (item.set_feat p 'type "pausativa"))
	    ((string-equal pu "F")
	     (item.set_feat p 'type "interrogativa"))
	    ((string-equal pu "E")
	     (item.set_feat p 'type "esclamativa"))
	    ((string-equal pu "G")
	     (item.set_feat p 'type "sospensiva"))
	    ((string-equal pu "H")
	     (item.set_feat p 'type "riportativa"))
	    (t 
	     (item.set_feat p 'type "BHO")))

	   
	   (set! p (item.next p)))))

(define (Simbolic_word_end_punct word)
  "(Simbolic_word_end_punct WORD)
Categorie di punctuation at end of related WORD, otherwise 0."
  (let ((p (word_end_punct word)))
    (cond 
     ((string-equal p "0") "0")
     ((or (string-equal p ".") (string-equal p ").") (string-equal p ".�") (string-equal p ".\"") (string-equal p ".-")) 'A)   
     ((or (string-equal p ";") (string-equal p ");") (string-equal p ";�") (string-equal p ";\"") (string-equal p ";-")) 'B)   
     ((or (string-equal p ",") (string-equal p "),") (string-equal p ",�") (string-equal p ",\"") (string-equal p ",-")) 'C)   
     ((or (string-equal p ":") (string-equal p "):") (string-equal p ":�") (string-equal p ":\"") (string-equal p ":-")) 'D)   
     ((or (string-equal p "!") (string-equal p ")!") (string-equal p "!�") (string-equal p "!\"") (string-equal p "!-")) 'E)   
     ((or (string-equal p "?") (string-equal p ")?") (string-equal p "?�") (string-equal p "?\"") (string-equal p "?-") (string-equal p "?).")  (string-equal p "...?�") (string-equal p "...?\"") ) 'F)
     ((or (string-equal p "..") (string-equal p "...") (string-equal p "...�") (string-equal p "...\"") ) 'G)
     ((or (string-equal p "-") (string-equal p "\"") (string-equal p "�") (string-equal p "\"") (string-equal p "\)") (string-equal p "\]") (string-equal p "\}")    ) 'H)   
     (t 'BHO))
    ))

(define (Simbolic_word_end_punct_2 word)
  "(Simbolic_word_end_punct WORD)
Categorie di punctuation at end of related WORD, otherwise 0."

  (let 
      ((p (word_end_punct word)))    
    (cond 
     ((string-equal p "0") "0")
     ((or (string-equal p ".") (string-equal p ").") (string-equal p ".�") ) "4")   
     ((or (string-equal p ";") (string-equal p ");") (string-equal p ";�") ) "3")   
     ((or (string-equal p ",") (string-equal p "),") (string-equal p ",�")  (string-equal p ",\"") ) "2")   
     ((or (string-equal p ":") (string-equal p "):") (string-equal p ":�") ) "3")   
     ((or (string-equal p "!") (string-equal p ")!") (string-equal p "!�") (string-equal p "!-") ) "4")   
     ((or (string-equal p "?") (string-equal p ")?") (string-equal p "?�") (string-equal p "?\"") (string-equal p "?-") (string-equal p "?).")  (string-equal p "...?�") ) "4")
     ((or (string-equal p "...") (string-equal p "...�") ) "3")
     ((or (string-equal p "-") (string-equal p "\"") (string-equal p "�") (string-equal p ")") ) "3")   
     (t 'BHO))
    ))

(define (Simbolic_word_start_prepunctuation word)
  "(Simbolic_word_start_prepunctuation WORD)
Categorie di prepunctuation at start of related WORD, otherwise 0."
  (let ((p (word_start_prepunctuation word)))
    (cond 
     ((string-equal p "0") "0")
     ((or (string-equal p "-") (string-equal p "\"") (string-equal p "�") (string-equal p "(") (string-equal p "(") (string-equal p "\[") (string-equal p "\{")) 'H)   
     (t 'BHO))
    ))

(define (KW_word word)
  ;; "1" se la parola � una key word
  
  (let ((r "0") k_w)
    (set! k_w italian_k_w)
    (while k_w
	   (if (string-equal (item.name word) (car k_w))
	       (set! r "1"))
	   (set! k_w (cdr k_w)))
    r))



;;-------------SILLABE----------------

(define (Tones_syl syl) 
  "(Tones_syl syl)"
  (item.feat syl 'R:Intonation_ToBI_merge.daughter1.name))

(define (Tones_List_syl syl)
  "Ritorna la lista di Toni tobi della sillaba"
  (if (item.relation syl 'Intonation_ToBI_merge)
      (let ((lista (item.daughters (item.relation syl 'Intonation_ToBI_merge))) elemTo LTones)
	(while lista 
	       (set! elemTo (item.feat (car lista) 'name))
	       (if (not (eq? elemTo 0)) 
		   (set! LTones (append LTones (list elemTo))))
	       (set! lista (cdr lista))) 
	(if LTones
	    LTones
	    0))
      0))

(define (Accenti_intonativi_syl syl)
  "Ritorna l'accento intonativo tobi della parola il car in quanto in una sillaba pu� al massimo contenere un accento intonativo e un tono di confine (quelli che non contengono ne - ne % ne !)"
  (if (item.relation syl 'Intonation_ToBI_merge) 
      (let ((lista (item.daughters (item.relation syl 'Intonation_ToBI_merge))) elemTo LTones)
	(while lista 
	       (set! elemTo (item.feat (car lista) 'name))
	       (if (and (not (eq? elemTo 0)) (not (string-matches elemTo ".*%.*")) (not (string-matches elemTo ".*-.*")) (not (string-matches elemTo ".*!.*")))
		   (set! LTones (append LTones (list elemTo))))
	       (set! lista  (cdr lista))) 
	(if LTones
	    (car LTones)
	    0))
      0))

(define (Boundary_syl syl)
  "Ritorna la lista dei boundaries tone  tobi della sillaba (sono quelli che non (non contengono ne - ne % ne ! ))"
  (if (item.relation syl 'Intonation_ToBI_merge)
      (let ((lista (item.daughters (item.relation syl 'Intonation_ToBI_merge))) elemTo LTones)
	(while lista 
	       (set! elemTo (item.feat (car lista) 'name))
	       (if (and (not (eq? elemTo 0)) (or (string-matches elemTo ".*%.*") (string-matches elemTo ".*-.*") (string-matches elemTo ".*!.*")))
		    (set! LTones (append LTones (list elemTo))))
	       (set! lista (cdr lista))) 
	(if LTones
	    (car LTones)
	    0))
      0))



(define (Toni_confine_syl syl)
  "Ritorna la lista dei toni di confine tobi della parola (sono quelli che finiscono per % )"
  (if (item.relation syl 'Intonation_ToBI_merge) 
      (let ( (lista (item.daughters (item.relation syl 'Intonation_ToBI_merge))) elemTo LTones)
	(while lista 
	       (set! elemTo (item.feat (car lista) 'name))
	       (if (and (not (eq? elemTo 0)) (string-matches elemTo ".*%"))
		   (set! LTones (append LTones (list elemTo))))
	       (set! lista (cdr lista))) 
	(car LTones))
      0))

(define (Accenti_frase_syl syl)
  "Ritorna la lista dei toni di confine tobi della parola (sono quelli che finiscono per - o inizianoo per %)"
  (if (item.relation syl 'Intonation_ToBI_merge) 
      (let ( (lista (item.daughters (item.relation syl 'Intonation_ToBI_merge))) elemTo LTones)
	(while lista 
	       (set! elemTo (item.feat (car lista) 'name))
	       (if (and (not (eq? elemTo 0)) (string-matches elemTo ".*-") (string-matches elemTo "%.*"))
		   (set! LTones (append LTones (list elemTo))))
	       (set! lista (cdr lista))) 
	(if LTones
	    (car LTones)
	    0))
      0))

(define (Downstep_syl syl)
  "Ritorna la lista dei donstep della parola (sono quelli che iniziano con ! "
  (if (item.relation syl 'Intonation_ToBI_merge) 
      (let ( (lista (item.daughters (item.relation syl 'Intonation_ToBI_merge))) elemTo LTones)
	(while lista 
	       (set! elemTo (item.feat (car lista) 'name))
	       (if (and (not (eq? elemTo 0)) (string-matches elemTo "!.*") )
		   (set! LTones (append LTones (list elemTo))))
	       (set! lista (cdr lista))) 
	(if LTones
	    (car LTones)
	    0))
      0))



;-------------------------SEGMENT-----------------------------------------
;;;;;;;;;;;;;;;;;FABIO;;;;;;;;;;;;;;;;;;;;
;;; consonant type: stop fricative affricative nasal liquid
;; (ctype s f a n l 0)
;; attezione sarebbe da copiare le caratteristiche afric e liquid 
;; nella voce italiana e anche (fw_current_01 seg) perch� se le mette nelle features del cart non le trova

(define (onset_stop seg)
  (onset_has_ctype seg "s"))
(define (onset_fric seg)
  (onset_has_ctype seg "f"))
(define (onset_afric seg)
  (onset_has_ctype seg "a"))
(define (onset_nasal seg)
  (onset_has_ctype seg "n"))
(define (onset_liquid seg)
  (onset_has_ctype seg "l"))

(define (coda_stop seg)
  (coda_has_ctype seg "s"))
(define (coda_fric seg)
  (coda_has_ctype seg "f"))
(define (coda_afric seg)
  (coda_has_ctype seg "a"))
(define (coda_nasal seg)
  (coda_has_ctype seg "n"))
(define (coda_liquid seg)
  (coda_has_ctype seg "l"))

(define (fw_current_01 seg)
  ;; "1" if word is funcion word 0 altrimenti
  (if (string-equal (item.feat seg 'R:SylStructure.parent.parent.gpos) "content")
	"0" ;;content 
	"1"
	)
)

(define (fw_prev_01 seg)
  ;; "1" if word is funcion word 0 altrimenti
  (if (string-equal (item.feat seg 'R:SylStructure.parent.parent.p.gpos) "content")
	"0" ;;content 
	"1"
	)
)

(define (fw_next_01 seg)
  ;; "1" if word is funcion word 0 altrimenti
  (if (string-equal (item.feat seg 'R:SylStructure.parent.parent.n.gpos) "content")
	"0" ;;content 
	"1"
	)
)


;-------------------------------------------------------
;OTHER

(define (KW_phrase Phrase)
  ;; "1" se nella frase c'� una key word
  
  (let ((word (item.daughter1 Phrase)) (r "0") k_w)
    (while word
	   (set! k_w italian_k_w)
	   (while k_w
		  (if (string-equal (item.name word) (car k_w))
		      (set! r "1"))
		  (set! k_w (cdr k_w)))
	   (set! word (item.next word)))
    r))


(set! italian_k_w
'(chi
che
cosa 
come
dove
perch�
perch�
quale
quando
quanto
)
)

(define (soleml_segment_duration seg)
"(soleml_segment_duration seg)"
(let ((dsd (item.feat seg 'segment_duration)))
  (if (< dsd 0)
      (item.feat seg 'end)
      dsd)))

(def_feature_docstring 'Syllable.lisp_next_stress
  "Syllable.lisp_next_stress
  Number of syllables to next stressed syllable. 0 if this syllable is
  stressed.  It is effectively assumed the syllable after the last syllable
  is stressed (tilt.scm untested). (GM)")
(define (next_stress syl)
  (cond 
   ((null syl) 0)
   ((string-equal (item.feat syl 'stress) "1")
    0)
   (t
    (+ 1 (next_stress (item.relation.next syl 'Syllable))))))

(def_feature_docstring 'Syllable.lisp_last_stress
  "Syllable.lisp_last_stress
  Number of syllables from previous stressed syllable.  0 if this syllable
  is stressed.  It is effectively assumed that the syllable before the 
  first syllable is stressed (tilt.scm untested). (GM)")
(define (last_stress syl)
  (cond 
   ((null syl) 0)
   ((string-equal (item.feat syl 'stress) "1")
    0)
   (t
    (+ 1 (last_stress (item.relation.prev syl 'Syllable))))))

(define (prima_geminata seg)
  (if (item.next seg)
      (cond 
       ((and 
	 (string-equal (item.feat seg 'ph_gem)  "+") ;;� geminabile
	 (string-equal (item.name seg) (item.feat seg 'n.name)) ;; � uguale al successivo
	 (item.next (item.parent (item.relation seg "SylStructure")))) ;; non � alla fine di una parola: esiste il successore della sillaba nella parola.  
	1)
       (t 0))
      0)
  )

(define (seconda_geminata seg)
  (if (item.prev seg)
      (cond 
       ((and 
	 (string-equal (item.feat seg 'ph_gem)  "+") ;;� geminabile 
	 (string-equal (item.name seg) (item.feat seg 'p.name)) ;; � uguale al precedente
	 (item.prev (item.parent (item.relation seg "SylStructure")))) ;; non � all' inizio di una parola: esiste il precedente della sillaba nella parola.  
        1)
       (t 0))
      0)
  )


(define (prima_geminata_stop seg)
  (if (item.next seg)
      (cond 
       ((and 
	 (string-equal (item.feat seg 'ph_ctype)  "s") ;;� stop
	 (string-equal (item.feat seg 'ph_gem)  "+") ;;� geminabile: inutile tutte le stop sono geminabili
	 (string-equal (item.name seg) (item.feat seg 'n.name)) ;; � uguale al successivo
	 (item.next (item.parent (item.relation seg "SylStructure")))) ;; non � alla fine di una parola: esiste il successore della sillaba nella parola.  
	1)
       (t 0))
      0)
  )

(define (seconda_geminata_stop seg)
  (if (item.prev seg)
      (cond 
       ((and 
	 (string-equal (item.feat seg 'ph_ctype)  "s") ;;� stop
	 (string-equal (item.feat seg 'ph_gem)  "+") ;;� geminabile 
	 (string-equal (item.name seg) (item.feat seg 'p.name)) ;; � uguale al precedente
	 (item.prev (item.parent (item.relation seg "SylStructure")))) ;; non � all' inizio di una parola: esiste il precedente della sillaba nella parola.  
	1)
       (t 0))
      0)
  )

(define (add_DIntensity_in_word utt)
"Add delta Intensity in word relation"
(let ((w (utt.relation.first utt 'Word)) (int (utt.relation.first utt 'DIntensity)))
  (while (and w int)
	 (while (or (string-equal (item.name int) "@BG") (string-equal (item.name int) "#"))
		(set! int (item.next int)))
	 (item.set_feat w 'DIntensity (item.feat int 'end))
	 (set! w (item.next w))
	 (set! int (item.next int)))))



	 