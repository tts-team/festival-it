(define (Classic_Pauses_BI utt)
  "(Pauses UTT)
Predict pause insertion."
  (let ((words (utt.relation.items utt 'Word)) lastword tpname)
    (if words
	(begin
	  (insert_initial_pause utt)   ;; always have a start pause
	  (set! lastword (car (last words)))
	  (mapcar
	   (lambda (w)
	     (let ((pbreak (item.feat w "pbreak"))
		   (emph (item.feat w "R:Token.parent.EMPH"))
		   (class_punct (token_end_punc w)))
	      (if (item.next w) 
		   (set! class_prepunctuation (token_start_punc (item.next w)))
		   (set! class_prepunctuation ""))
	      (cond
	       ((or (and (string-equal "mB" pbreak) (or (not (string-equal "0" class_punct)) (not (string-equal "" class_prepunctuation))))	    
		    (and (string-equal "B" pbreak) (or (not (string-equal "0" class_punct)) (not (string-equal "" class_prepunctuation)))) 
		    (and (string-equal "BB" pbreak) (or (not (string-equal "0" class_punct)) (not (string-equal "" class_prepunctuation)))))
		 (insert_pause utt w))
;		((string-equal emph "1")
;		 (insert_pause utt w))
		((equal? w lastword)
		 (insert_pause utt w)))))
	   words)
	  ;; The embarassing bit.  Remove any words labelled as punc or fpunc
	  (mapcar
	   (lambda (w)
	     (let ((pos (item.feat w "pos")))
	       (if (or (string-equal "punc" pos)
		       (string-equal "fpunc" pos))
		   (let ((pbreak (item.feat w "pbreak"))
			 (wp (item.relation w 'Phrase)))
		     (if (and (string-matches pbreak "BB?")
			      (item.relation.prev w 'Word))
			 (item.set_feat 
			  (item.relation.prev w 'Word) "pbreak" pbreak))
		     (item.relation.remove w 'Word)
		     ;; can't refer to w as we've just deleted it
		     (item.relation.remove wp 'Phrase)))))
	   words)))
  utt))


(define (BI_Word utt)
"Predice il BI ToBI usando phrase_BI_tree"
(let ((w (utt.relation.first utt 'Word)))
  (while w
	 (item.set_feat w 'BI (wagon_predict w phrase_BI_tree))
	 (set! w (item.next w)))))


(define (PaIntE_CW_NUM_syl utt)
"Predice il CW_NUM PaIntE usando PaIntE_CW_NUM_tree"
(let ((s (utt.relation.first utt 'Syllable)))
  (while s 
	 (if (not (string-equal (item.feat s 'R:SylStructure.lisp_Accent_boundary_syl) "s"))
	     (item.set_feat s 'CW_NUM (wagon_predict s PaIntE_CW_NUM_tree)))
	     
	 (set! s (item.next s)))))


(define (PaIntE_CW_vq_syl utt)
"(PaIntE_CW_vq_syl utt) 
Predice il CW_NUM PaIntE usando PaIntE_CW_NUM_tree"
(let ((s (utt.relation.first utt 'Syllable)))
  (while s 
	 (if (not (string-equal (item.feat s 'R:SylStructure.lisp_Accent_boundary_syl) "s"))
	     (let () 
	       (if (defvar debug_mode) 
		   (if debug_mode 
		       (print (wagon_predict s PaIntE_CW_vq_tree))))
	       (set_painte utt s (wagon_predict s PaIntE_CW_vq_tree)) ) ) 	     
	 (set! s (item.next s)))))


(define (male2female fm) 
  (if (not (defvar pitch_male2female_conversion))
      (set! pitch_male2female_conversion  nil))
  (if pitch_male2female_conversion
      (let ((a 136,5) (c -428.8))
	;;logaritmo su due punti 100->200; 300->350
	;;ff=a*log(fm)+c
	(+ c (* a (log fm))))
      fm      ))

(define (mel2hz mel)
(* 700 (- (exp (/ mel 1127.01048)) 1))) 


(define (hz2mel hz)
(* (log (+ 1 (/ hz 700)))  1127.01048))




(define (mel2frq_target utt)
"converte e streach di pitch in Target relation"
;mel_streach
;;;
(if (not (defvar mel_streach))
    (set! mel_streach 1))
(if (not (defvar hz_streach))
    (set! hz_streach 1))



(mapcar
 (lambda (segment) 
   (mapcar
    (lambda (targ_item)
      (let ()
	;;HZ=700*(exp(mel/1127.01048)-1);
	(item.set_feat targ_item "f0" (male2female (* hz_streach (* 700 (- (exp (/ (* (item.feat targ_item "f0") mel_streach) 1127.01048) ) 1) ))) )))
      (item.relation.daughters segment 'Target))
   ) 
 ;; list of targets
 ;;(list   
 ;; (item.feat segment "name")))
 (utt.relation.items utt 'Segment))
)

;;;
(define (list-elem_n list n)
; first n elements of the list 
; 0 � il primo!
  (cond 
   ((eq? n 0) (car list))
   (t (list-elem_n (cdr list) (- n 1)))))


(define (Set_PaIntE_CW_NUM_syl utt)
"Setta i parametri PaIntE CW in tutte le sillabe."
(let ((s (utt.relation.first utt 'Syllable))(num 1))
  (while s
	 (if (not (eq? (item.feat s 'CW_NUM) 0))
	     (let ()
	     (set_painte utt s (list-elem_n PaIntE_file_CB (- (item.feat s 'CW_NUM) 1)))
	     ;(print (item.feat s 'CW_NUM))
	     (print (item.feat s 'CW_NUM))
	     ;(print (list-elem_n PaIntE_file_CB (- (item.feat s 'CW_NUM) 1)))
	     ))
	     ;(print num)
	     ;(set! num (+ num 1))
	     (set! s (item.next s)))))




;; Questo tiene anche in considerazione l'apostrofo in quanto dell'amore � trascritto come  (((d e l) 0) ((l a) 0) ((m o1) 1) ((r e) 0)) grazie all'if nell lts che � implicito che le parole prima dell'apostrofo sono function word. (Da mettere anche in flite) Il gpos e la pos � presa dalla seconda parte della parola. Sarebbe da vedere anche se mettere tutte le function word trascritte senza accento... e utilizzare le regole di trascrizione lts senza quest if in modo che il difono usato sia a1, e1 ...
 
(set! simple_italian_accent_tree
 '
  ((R:SylStructure.parent.gpos is content)
   ((stress is 1)
    ((Accented))
    ((NONE)))
   ((NONE))))

