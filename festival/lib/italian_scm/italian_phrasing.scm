;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                        ;;
;;                Istituto di Fonetica e Dialettologia                    ;;
;;                 Consiglio Nazionale delle Ricerche                     ;;
;;                            Padova Italy                                ;;
;;                        Copyright (c) 2000                              ;;
;;                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        						  ;;
;;           Centro per la ricerca scientifica e tecnologica              ;;
;;         	             Povo (TN) - Italy				  ;;
;;			    Copyright (c) 2000                            ;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;
;;; PHRASE BREAKS ;;;
;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                                       ;;;
;;; Cart-tree delle pause costruito in base alla punteggiatura che segue  ;;;
;;; il "token"  e al campo 'name' del "token" successivo                  ;;;
;;;                                                                       ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (token_start_punc word)
  "(token_start_punc UTT WORD)
If prepunctuation at start of related Token and if WORD is last word
in Token return prepunpunc, otherwise 0."
  (if (item.relation.next word "Token")
      "0"
      (item.feat word "R:Token.parent.prepunctuation")))

;; se si vuole si pu� agiungere un altro campo mB una pausa ancora + piccola di B

(set! italian_phrase_cart_tree_old
'
((lisp_token_end_punc in ("?" "." ":" "!" ".." "..." "?-" ".-" ":-" "!-" "..-" "...-" "?>>" ".>>" ":>>" "!>>" "..>>" "...>>"".\"" "!\"" "?\"" "...\"" ))
  ((BB))
  ((lisp_token_end_punc in (";" ";-" "-" ";>>" ">>" ";\"" )) ;ho tolto "'" 
    ((B))
    ((lisp_token_end_punc in ("," ))
     ((mB))
     ((lisp_token_end_punc in (")" "}" "]" "\"" ",-" ")-" "}-" "]-" "\"-" ",\"" ",>>" ")>>" "}>>" "]>>" "\">>"))
      ((B))
      ((n.lisp_token_start_punc in ("(" "{" "[" "\"" "(-" "{-" "[-" "\"-" "-" "(<<" "{<<" "[<<" "\"<<" "<<"))
       ((B))
       ((n.name is 0)  ;; end of utterance
	((BB))
	((NB)))))))))



;;modificato resta solo virgola in mB


;lo stesso ma senza in (non compatibile con make_cart):
(set! flite_italian_phrase_cart_tree3
'
((Simbolic_word_end_punct is "A")
 ((BB))
 ((Simbolic_word_end_punct is "D")
  ((BB))
  ((Simbolic_word_end_punct is "E")
   ((BB))
   ((Simbolic_word_end_punct is "F")
    ((BB))
    ((Simbolic_word_end_punct is "G")
     ((BB))
     ((Simbolic_word_end_punct is "H")
      ((BB))
      ((Simbolic_word_end_punct is "B") 
       ((B))
       ((Simbolic_word_end_punct is "C")
	((mB))
	((n.Simbolic_word_start_prepunctuation is "H")
	 ((B))
	 ((R:Token.parent.n.name is "--")
	  ((BB))
	  ((break is "1")
	   ((BB))
	   ((R:Token.parent.break is "1")
	    ((BB))
	    ((n.name is 0)
	     ((BB))
	     ((NB))))))))))))))))  

  

;lo stesso ma senza in (non compatibile con make_cart):
(set! italian_phrase_cart_tree
'
((lisp_Simbolic_word_end_punct is "A")
 ((BB))
 ((lisp_Simbolic_word_end_punct is "D")
  ((BB))
  ((lisp_Simbolic_word_end_punct is "E")
   ((BB))
   ((lisp_Simbolic_word_end_punct is "F")
    ((BB))
    ((lisp_Simbolic_word_end_punct is "G")
     ((BB))
     ((lisp_Simbolic_word_end_punct is "H")
      ((BB))
      ((lisp_Simbolic_word_end_punct is "B") 
       ((B))
       ((lisp_Simbolic_word_end_punct is "C")
	((mB))
	((n.lisp_Simbolic_word_start_prepunctuation is "H")
	 ((B))
	 ((n.name is 0)
	  ((BB))
	  ((NB)))))))))))))  

(provide 'italian_phrasing)