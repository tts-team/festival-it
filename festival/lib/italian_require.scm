;; ADD italian_scm to the path
(set! lib_scm_dir (path-append libdir "italian_scm/"))
(set! load-path (cons lib_scm_dir load-path))

(set! synthesis_reset_file (path-append libdir "synthesis.scm"))

;; Load any common required files
(require 'italian_phoneset)
(require 'italian_lts)
(require 'italian_lexicon)
(require 'italian_lexicon_irst)
(require 'irst2sampa)
(require 'italian_phrasing)
(require 'italian_token)
(require 'italian_oredatetel)
(require 'italian_gpos)
(require 'italian_module)
(require 'ita_map)

;; Functions
(require 'fdefine)
(require 'functions)

(require 'italian_mbrola)

;APML
(Parameter.set 'Emotive_Post_Method 'Emotive_One)
(require 'apml-mode)

(provide 'italian_require)
