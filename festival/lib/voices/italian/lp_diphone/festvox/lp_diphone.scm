;; ===================================================================================
;; Italian FESTIVAL Modules(IFM)
;; Copyright (C) 2001-2007 by the IFM Development Team
;; at "ISTC-SPFD CNR" and at "ITC-Irst".
;; ===================================================================================
;;	ISTC-SPFD CNR
;;		Istituto di Scienze e Tecnologie della Cognizione
;;		Sezione di Padova "Fonetica e Dialettologia"
;;		Consiglio Nazionale delle Ricerche
;;		Via Martiri della libert�, 2 - 35137 Padova
;;		tel (+39) 049 8271827 - fax (+39) 049 8271825
;;		e-mail: segreteria@pd.istc.cnr.it 
;; 
;;	ITC-irst
;;		Istituto Trentino di Cultura
;;		Centro per la ricerca scientifica e tecnologica 
;;		Via Santa Croce 77 - 38100 Trento ITALIA
;;		tel (+39) 0461 210111 - fax (+39) 0461 980436
;;		e-mail: info@itc.it 
;; ===================================================================================
;; This file is part of IFM.
;; 
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
;; ===================================================================================
;; Authors:	Piero COSI, ISTC-SPFD CNR, (cosi@pd.istc.cnr.it)
;; 		Carlo DRIOLI
;; 		Graziano TISATO
;; 		Giulio PACI
;; 		Roberto GRETTER, ITC-irst (SSI/MPA), (gretter@itc.it) 
;; 		Fabio TESSER
;; ===================================================================================
;;		WEB:  http://www.pd.istc.cnr.it/TTS/ItalianFESTIVAL
;; ===================================================================================
;;
;;  Set up lp_diphone using the standard UniSyn diphone synthesizer
;;  Loredana Panato diphones: female Italian collected February 2000 at IRST
;;
;; ===================================================================================
;;
;;
(defvar lp_diphone_dir (cdr (assoc 'lp_diphone voice-locations)))
(set! load-path (cons (path-append lp_diphone_dir "festvox/") load-path))



;Load italian scm files
(require 'italian_require)
(require_module 'UniSyn)

;APML voice quality maps
(require 'maps_lp)

;; CART files...
(require 'ifd_diph_ds_dur)
(require 'italian_intonation)

;; DEFINIZIONE di UNGRUPPED MODE
;; ATTENZIONE INSERIRE QUI LE DIRECTORY DEI FILE LPC PER UNGRUPPED!!!
(set! lp_lpc_sep 
      (list
      '(name "lp_lpc_sep")
      ;NO_LAR_MODE
      (list 'index_file (path-append lp_diphone_dir "dic/lp_diph_mbrola_pda_correct.est"))
      ;LAR_MODE
      ;(list 'index_file (path-append lp_diphone_dir "dic/lp_diph_lar.est"))
      '(grouped "false")
      ;(list 'coef_dir (path-append lp_diphone_dir "lpc"))
      ;(list 'sig_dir  (path-append lp_diphone_dir "lpc"))
      ;(list 'coef_dir 'H:\Sintesi\Data\PieroCosi\Lpc)
      ;(list 'sig_dir  'H:\Sintesi\Data\PieroCosi\Lpc)
      ;NO_LAR_MODE
      (list 'coef_dir 'H:\Sintesi\Data\LoredanaPanato\reduced\lpc_est)
      (list 'sig_dir  'H:\Sintesi\Data\LoredanaPanato\reduced\lpc_est)
      ;LAR_MODE
      ;(list 'coef_dir 'H:\Sintesi\Data\PieroCosi\Lpc_lar)
      ;(list 'sig_dir  'H:\Sintesi\Data\PieroCosi\Lpc_lar)
      '(coef_ext ".lpc")
      '(sig_ext ".res")
      '(default_diphone "#-#")))
;; DEFINIZIONE di GRUPPED MODE 
(set! lp_lpc_group 
      (list
       '(name "lp_lpc_group")
       (list 'index_file 
	     (path-append lp_diphone_dir "group/lp_diphone.group"))
       '(grouped "true")
       '(default_diphone "#-#")))

;; Go ahead and set up the diphone db
;;;SCEGLI TRA GRUPPED MODE O UNGRUPPED MODE TOGLIENDO I COMMENTI DALLA RIGA RELATIVA (1)
;;GRUPPED MODE
(us_diphone_init lp_lpc_group)
;;UNGRUPPED MODE
;;(us_diphone_init lp_lpc_sep)

;;VOICE ResEt....
(define (italian_voice_reset)
  "(italian_voice_reset)
Reset global variables back to previous voice."
  (set! token.prepunctuation italian_previous_tok_prepunc)
  (set! token.punctuation italian_previous_tok_punc) ;F_MOD per l'italiano devo toglier l'accento ' dalla punteggiatura e non dalla prepunteggiatura
  (load synthesis_reset_file);;per togliere i set di italian_module da noi utilizzati
)

;;;  Full voice definition 
(define (voice_lp_diphone)
"(voice_italian_lp)
Set up synthesis for Female Italian speaker: Loredana Panato"
  (voice_reset)
  (Parameter.set 'Language 'italian)
  ;; Phone set
  (Parameter.set 'PhoneSet 'italian)
  (PhoneSet.select 'italian)

  ;; numeric expansion
  (Parameter.set 'Token_Method 'Token_Any)
  (set! token_to_words italian_token_to_words)

  ;; Because of use of ' for accents remove it from prepunctuation
  (set! italian_previous_tok_prepunc token.prepunctuation)
  (set! italian_previous_tok_punc token.punctuation)
  (set! token.prepunctuation "\"'`({[")  ;)F_MOD ;default "\"'`({[")
  (set! token.punctuation "\".,:;!?(){}[]") ;F_MOD ;default "\"'`.,:;!?(){}[]" ....fabio ho tolto '`
  (set! token.whitespace " \t\n\r") ;fabio ha tolto il meno da white space per i numeri

  ;; Postlexical rules
  (set! postlex_vowel_reduce_cart_tree  nil)
  (set! postlex_rules_hooks nil)

  ;; No pos prediction (get it from lexicon)
  (set! pos_lex_name nil)
  ;; Phrase break prediction by punctuation
  (set! pos_supported nil) ;; well not real pos anyhow
  ;; Phrasing
  (set! phrase_cart_tree italian_phrase_cart_tree)
  (Parameter.set 'Phrase_Method 'cart_tree)

  ;; Lexicon selection
  ;(lex.select "italian")
  (lex.select "otherlex")
  ;(lex.select "otherlex_irst")

  
;;;;;;; F0 prediction ;;;;;;;;;;;;;;; 
;  (set! f0_lr_start f2b_f0_lr_start)
;  (set! f0_lr_mid f2b_f0_lr_mid)
;  (set! f0_lr_end f2b_f0_lr_end)
;  (Parameter.set 'Int_Method Intonation_Tree)

;;;;;;;;GENERAL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;; Accent and tone prediction
 (set! int_accent_cart_tree italian_accent_cart_tree)
 (Parameter.set 'Int_Target_Method 'Simple)
 (Parameter.set 'Int_Method 'General)
  ;;INTONAZIONE F_W+Question
  (set! int_general_params (list (list 'targ_func targ_func_fw_q_fem)))
  
;;;;;;;;;;;;;;;;;;;;;;FINE GENERAL;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;;Part of speech Function Word
  (set! guess_pos italian_guess_pos)

  ;; Pauses prediction
  (Parameter.set 'Pause_Method Classic_Pauses_mB)

;;;;;;;;;;;; Duration prediction ;;;;;;;;;;;;;;
;; David Sassoli ;;
 (set! duration_cart_tree ifd_diph_ds::zdurtree)
 (set! duration_ph_info ifd_diph_ds::phone_durs)
  (Parameter.set 'Duration_Method 'Tree_ZScores)

  ;; Waveform synthesizer: diphones
  (set! us_abs_offset 0.0)
  (set! window_factor 1.0)
  (set! us_rel_offset 0.0)
  (set! us_gain 0.9)

  (Parameter.set 'Synth_Method 'UniSyn)
  (Parameter.set 'us_sigpr 'lpc)
  
  ;;;SCEGLI TRA GRUPPED MODE O UNGRUPPED MODE TOGLIENDO I COMMENTI DALLA RIGA RELATIVA (2)
  ;;GRUPPED MODE
  (us_db_select 'lp_lpc_group)
  ;;UNGRUPPED MODE
  ;;(us_db_select 'lp_lpc_sep)

  (require 'italian_module_flush)
  ;; set callback to restore some original values changed by the italian voice
  (set! current_voice_reset italian_voice_reset)
  (set! current-voice 'lp_diphone)
)


(proclaim_voice
 'lp_diphone
 '((language italian)
   (gender female)
   (dialect none)
   (description
    "This voice provides a Italian female voice using a
     residual excited LPC diphone synthesis method.  The lexicon
     is provived by a set of letter to sound rules producing pronunciation
     accents and syllabification.  The durations, intonation and
     prosodic phrasing are minimal but are acceptable for simple
     examples.")
     (coding ISO-8859-1)))

(provide 'lp_diphone)
